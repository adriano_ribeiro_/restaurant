<?php


use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;



return simpleDispatcher(function (RouteCollector $route) {

    // 'auth' é o indentificador para verificar se o usuário tá logado,
    // vc compara ele la no index.php como se fosse uma middleware
    //$route->addRoute('GET', '/', ['app\view\UserView', 'index', 'auth']);
    $route->addRoute('GET', '/', ['app\view\UserView', 'index']);

    // rotas view employee
    $route->addGroup('/funcionario', function (RouteCollector $route) {
        $route->get('/cadastro', ['app\view\EmployeeView', 'create']);
        $route->get('s', ['app\view\EmployeeView', 'active']);
    });

    // rota action employee
    $route->addGroup('/employee', function (RouteCollector $route) {
        $route->post('/store', 'app\controller\EmployeeController::store');
        $route->post('/update/{id}', 'app\controller\EmployeeController::update');
        $route->post('/update/status/{id}', 'app\controller\EmployeeController::updateStatus');
        $route->get('/details/{id}', 'app\controller\EmployeeController::details');
        $route->get('/active', 'app\controller\EmployeeController::active');
        $route->get('/inactive', 'app\controller\EmployeeController::inactive');
        $route->get('/all/waiter/active', 'app\controller\EmployeeController::allWaiterActive');
    });

    // rotas view user
    $route->addGroup('/usuario', function (RouteCollector $route) {
        $route->get('/cadastro', ['app\view\UserView', 'register']);
        $route->get('/login', 'app\view\UserView::login');
        $route->get('/cliente', 'app\view\UserView::client');
        $route->get('s', 'app\view\UserView::users');
    });

    // rota action user
    $route->addGroup('/user', function (RouteCollector $route) {
        $route->post('/auth', 'app\controller\UserController::auth');
        $route->get('/logout', 'app\controller\UserController::logout');
        $route->post('/store', 'app\controller\UserController::store');
        $route->post('/update/{id}', 'app\controller\UserController::update');
        $route->get('/active', 'app\controller\UserController::active');
    });

    // rota action functionPerson
    $route->addGroup('/function', function (RouteCollector $route) {
        $route->post('/store', 'app\controller\FunctionPersonController::store');
        $route->get('/active', 'app\controller\FunctionPersonController::active');
    });

    // rota view board
    $route->addGroup('/mesa', function (RouteCollector $route) {
        $route->get('/cadastro', 'app\view\BoardView::create');
        $route->get('s', 'app\view\BoardView::active');
    });

    // rota action board
    //todo => POSSO USAR ASSIM TBM.
    //$route->addRoute('POST', '/board/store', ['app\controller\BoardController', 'store']);
    //$route->post('/board/store', ['app\controller\BoardController', 'store']);
    $route->addGroup('/board', function (RouteCollector $route) {
        //$route->addRoute('POST', '/store', ['app\controller\BoardController', 'store']);
        $route->post('/store', 'app\controller\BoardController::store');
        $route->get('/active', 'app\controller\BoardController::active');
        $route->get('/inactive', 'app\controller\BoardController::inactive');
        $route->get('/details/{id}', 'app\controller\BoardController::details');
        $route->post('/update/{id}', 'app\controller\BoardController::update');
        $route->post('/update/status/{id}', 'app\controller\BoardController::updateStatus');
        $route->post('/update/status/true/{id}', 'app\controller\BoardController::updateStatusTrue');
    });

    // rota view product
    $route->addGroup('/produto', function (RouteCollector $route) {
        $route->get('/cadastro', 'app\view\ProductView::create');
        $route->get('s', 'app\view\ProductView::products');
    });

    // rota action product
    $route->addGroup('/product', function (RouteCollector $route) {
        $route->post('/store', 'app\controller\ProductController::store');
        $route->post('/update/{id}', 'app\controller\ProductController::update');
        $route->get('/active/sale', 'app\controller\ProductController::productActiveSale');
        $route->get('/active', 'app\controller\ProductController::active');
        $route->get('/inactive', 'app\controller\ProductController::inactive');
        $route->get('/details/{id}', 'app\controller\ProductController::details');
        $route->post('/update/status/{id}', 'app\controller\ProductController::updateStatus');
    });

    // rota view box
    $route->addGroup('/venda', function (RouteCollector $route) {
        $route->get('/caixa', 'app\view\BoxView::box');
    });

    // rota action box
    $route->addGroup('/box', function (RouteCollector $route) {
        $route->post('/store', 'app\controller\BoxController::store');
//        $route->get('/value/all', 'app\controller\BoxController::valueAll');
        $route->get('/details/sale', 'app\controller\BoxController::detailsSale');
    });

    // rota view sale
    $route->addGroup('/pedidos', function (RouteCollector $route) {
        $route->get('', 'app\view\SaleView::sale');
        $route->get('/aberto', 'app\view\SaleView::openSale');
        $route->get('/fechado', 'app\view\SaleView::closedSale');
//        $route->get('/detalhes/{id}', 'app\view\SaleView::detailsSaleActive');
        $route->get('/detalhes', 'app\view\SaleView::detailsSaleActive');
    });

    // rota action sale
    $route->addGroup('/sale', function (RouteCollector $route) {
        $route->get('/active', 'app\controller\SaleController::saleActive');
        $route->get('/details/{id}', 'app\controller\SaleController::detailsSale');
        $route->get('/boards/{id}', 'app\controller\SaleController::getBoardsBySale');
        $route->get('/count/active', 'app\controller\SaleController::countSaleActive');
        $route->get('/value/all/{id}', 'app\controller\SaleController::getValueBySale');
        $route->get('/finish/{id}', 'app\controller\SaleController::finishSale');
    });

    // rota action group
    $route->addGroup('/group', function (RouteCollector $route) {
        $route->get('/sale/active', 'app\controller\GroupController::saleActive');
        $route->get('/sale/inactive', 'app\controller\GroupController::saleInactive');
        $route->get('/sale/details/{id}', 'app\controller\SaleController::details');
    });
});