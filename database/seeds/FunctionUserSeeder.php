<?php


use Phinx\Seed\AbstractSeed;


/**
 * Class FunctionUserSeeder
 */
class FunctionUserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            'name' => 'cozinheira',
            'status' => true,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        //$this->insert('function', $data);
    }
}





