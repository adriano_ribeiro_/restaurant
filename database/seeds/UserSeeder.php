<?php


use app\util\Password;
use Phinx\Seed\AbstractSeed;


/**
 * Class UserSeeder
 */
class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
            $data = [
                'name'          => 'Júnior',
                'email'         => 'junior@gmail.com',
                'password'      => Password::hash('1234'),
                'function_id'   => 3,
                'status'        => true,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            ];

        //$this->insert('users', $data);
    }
}
