<?php

use Phinx\Migration\AbstractMigration;

class CreateGroupSale extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $group = $this->table('groups_sales', ['id' => false]);
        $group
            ->addColumn('group_id', 'biginteger')
            ->addColumn('sale_id', 'biginteger')
            ->addForeignKey('group_id', 'groups', ['id'])
            ->addForeignKey('sale_id', 'sales', ['id'])
            ->create();
    }
}
