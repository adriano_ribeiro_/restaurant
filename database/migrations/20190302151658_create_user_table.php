<?php


use Phinx\Db\Table\Column;
use Phinx\Migration\AbstractMigration;

class CreateUserTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $column = new Column();
        $column->setName('id')
            ->setType('biginteger')
            ->setIdentity(true);

        $options = array(
            'id'          => false,
            'primary_key' => 'id'
        );

        $users = $this->table('users', $options);
        $users
            ->addColumn($column)
            ->addColumn('email', 'string', ['limit' => 80])
            ->addColumn('password', 'string', ['limit' => 100])
            ->addColumn('person_id', 'biginteger')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->addIndex(['email'], ['unique' => true])
            ->addForeignKey('person_id', 'people', ['id'])
            ->create()
        ;
    }
}
