<?php


use Phinx\Db\Table\Column;
use Phinx\Migration\AbstractMigration;

class CreateBoxTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $column = new Column();
        $column->setName('id')
            ->setType('biginteger')
            ->setIdentity(true);

        $options = array(
            'id' => false,
            'primary_key' => 'id'
        );

        $box = $this->table('boxes', $options);
        $box
            ->addColumn($column)
            ->addColumn('product_id', 'biginteger')
            ->addColumn('quantity', 'biginteger')
            ->addColumn('subtotal', 'float', ['limit' => '4,2', 'default' => 0])
            ->addColumn('sale_id', 'biginteger')
            ->addColumn('employee_id', 'biginteger')
            ->addColumn('created_at', 'datetime')
            ->addColumn('updated_at', 'datetime')
            ->addForeignKey('product_id', 'products', ['id'])
            ->addForeignKey('sale_id', 'sales', ['id'])
            ->addForeignKey('employee_id', 'employees', ['id'])
            ->create();
    }
}
