<?php


use Phinx\Db\Table\Column;
use Phinx\Migration\AbstractMigration;

class CreatePersonTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $column = new Column();
        $column->setName('id')
            ->setType('biginteger')
            ->setIdentity(true);

        $options = array(
            'id'          => false,
            'primary_key' => 'id'
        );

        $users = $this->table('people', $options);
        $users
            ->addColumn($column)
            ->addColumn('name', 'string', ['limit' => 80])
            ->addColumn('function_id', 'biginteger')
            ->addColumn('status', 'boolean', ['limit' => 1])
            ->addIndex(['name'], ['unique' => true])
            ->addForeignKey('function_id', 'functions', ['id'])
            ->create()
        ;
    }
}
