<?php

use Phinx\Migration\AbstractMigration;

class CreateGroupBoard extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $group = $this->table('groups_boards', ['id' => false]);
        $group
            ->addColumn('group_id', 'biginteger')
            ->addColumn('board_id', 'biginteger')
            ->addForeignKey('group_id', 'groups', ['id'])
            ->addForeignKey('board_id', 'boards', ['id'])
            ->create();
    }

//$refTable = $this->table('post_tag');
//$refTable->addColumn('tag_id', 'integer', ['null' => true])
//->addColumn('post_id', 'integer', ['null' => true])
//->addForeignKey('tag_id', 'tags', 'id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
//->addForeignKey('post_id', 'posts', 'id', ['delete'=> 'CASCADE'', 'update'=> 'NO_ACTION'])
//                        ->save();
}
