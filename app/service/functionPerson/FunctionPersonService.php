<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 03/02/2019
 * Time: 14:34
 */

namespace app\service\functionPerson;


use app\exception\ValidationException;
use app\factory\FunctionPersonFactory;
use app\model\FunctionPerson;
use app\repository\functionPerson\FunctionPersonRepository;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class FunctionPersonService
 * @package app\service\functionPerson
 */
class FunctionPersonService extends Json implements FunctionPersonServiceInterface
{
    use ValidatorTrait;

    /** @var EntityManagerInterface */
    private $em;

    /** @var FunctionPersonRepository */
    private $functPersonRepository;

    /**
     * FunctionPersonService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->functPersonRepository= $this->em->getRepository(FunctionPerson::class);
    }


    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $functPerson = FunctionPersonFactory::make($data);
            $this->valid($functPerson);
            $this->functPersonRepository->save($functPerson);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $functPerson->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->warning($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     */
    public function update(int $id)
    {
        // TODO: Implement update() method.
    }

    /**
     * @return array
     */
    public function findActive()
    {
        try {
             return $this->success($this->codeSuccess, $this->messageSuccess, $this->functPersonRepository->findActive());
        } catch(Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }

    /**
     * @return array
     */
    public function findInactive()
    {
        return [];
    }
}