<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 03/02/2019
 * Time: 14:32
 */

namespace app\service\functionPerson;


interface FunctionPersonServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function findActive();

    public function findInactive();

}