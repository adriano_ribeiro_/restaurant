<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:22
 */

namespace app\service\group;


/**
 * Interface GroupServiceInterface
 * @package app\service\group
 */
interface GroupServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @return mixed
     */
    public function saleActive();

    /**
     * @param int $id
     * @return mixed
     */
    public function groupSale(int $id);
}