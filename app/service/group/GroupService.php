<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:23
 */

namespace app\service\group;


use app\exception\ValidationException;
use app\factory\GroupFactory;
use app\model\Board;
use app\model\Group;
use app\repository\board\BoardRepository;
use app\repository\group\GroupRepository;
use app\service\board\BoardServiceInterface;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class GroupService
 * @package app\service\group
 */
class GroupService extends Json implements GroupServiceInterface
{
    use ValidatorTrait;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var GroupRepository
     */
    private $groupRepository;

    /** @var BoardRepository */
    private $boardRepository;

    /** @var BoardServiceInterface */
    private $boardService;

    /** @var Group */
    private $group;

    /**
     * GroupService constructor.
     * @param EntityManagerInterface $em
     * @param BoardServiceInterface $boardService
     */
    public function __construct(EntityManagerInterface $em, BoardServiceInterface $boardService)
    {
        $this->em = $em;
        $this->groupRepository = $this->em->getRepository(Group::class);
        $this->boardRepository = $this->em->getRepository(Board::class);
        $this->boardService = $boardService;
    }


    /**
     * @param array $data
     * @return Group|array
     */
    public function save(array $data)
    {
        try {
            $this->group = GroupFactory::make($data);
            $this->valid($this->group);
            $this->groupRepository->save($this->group);
            return $this->group;
        } catch (ValidationException $e) {
            throw new $e;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return array
     */
    public function saleActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->groupRepository->saleActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array|mixed
     */
    public function groupSale(int $id)
    {
        try {
            return $this->groupRepository->groupSale($id);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}