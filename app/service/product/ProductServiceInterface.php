<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 13:54
 */

namespace app\service\product;


interface ProductServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function findActive();

    public function findInactive();

    public function productActiveSale();

    public function details(int $id);
}