<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 13:56
 */

namespace app\service\product;


use app\exception\ValidationException;
use app\factory\ProductFactory;
use app\model\Product;
use app\repository\product\ProductRepository;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Kint\Kint;
use Throwable;


/**
 * Class ProductService
 * @package app\service\product
 */
class ProductService extends Json implements ProductServiceInterface
{
    use ValidatorTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var Product */
    private $product;

    /**
     * @var ProductRepository
     */
    private $productRepository;


    /**
     * ProductService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->productRepository = $this->em->getRepository(Product::class);
    }


    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $product = ProductFactory::make($data);
            $this->valid($product);
            $this->productRepository->save($product);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $product->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->product = $this->productRepository->getReference($id);
            $this->valid(
                    $this->product
                    ->setName($_POST['name'])
                    ->setDescription($_POST['description'])
                    ->setPrice($_POST['price'])
            );
            $this->productRepository->save($this->product);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->product->getName());
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function updateStatus(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->product = $this->productRepository->getReference($id);
            $this->valid($this->product->setStatus($_POST['status']));
            $this->productRepository->save($this->product);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->product->getName());
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function productActiveSale()
    {
        try {
            $products = $this->productRepository->productActiveSale();
            $array = array();
            foreach ($products as $product) {
                $data = array(
                    "id" => $product['id'],
                    "text" => $product['name'] . ' - ' . 'R$ ' . number_format($product['price'], 2, ',', '.')
                );
                array_push($array, $data);
            }
            return $this->success($this->codeSuccess, $this->messageSuccess, $array);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function findActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->productRepository->findActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function findInactive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->productRepository->findInactive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function details(int $id)
    {
        try {
            $this->product = $this->productRepository->getReference($id);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->product->jsonSerialize());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), null);
        }
    }
}