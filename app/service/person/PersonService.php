<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 11:43
 */

namespace app\service\person;


use app\exception\ValidationException;
use app\factory\PersonFactory;
use app\model\FunctionPerson;
use app\model\Person;
use app\repository\functionPerson\FunctionPersonRepository;
use app\repository\person\PersonRepository;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class PersonService
 * @package app\service\person
 */
class PersonService extends Json implements PersonServiceInterface
{
    use ValidatorTrait;


    /** @var EntityManagerInterface */
    private $em;

    /** @var PersonRepository */
    private $personRepository;

    /** @var FunctionPersonRepository */
    private $functPersonRepository;

    /** @var FunctionPerson */
    private $functPerson;

    /** @var Person */
    private $person;

    /**
     * PersonService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->personRepository = $this->em->getRepository(Person::class);
        $this->functPersonRepository = $this->em->getRepository(FunctionPerson::class);
    }


    /**
     * @param array $data
     * @return Person|array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->functPerson = $this->functPersonRepository->getReference($data['function_id']);
            $this->person = PersonFactory::make(array_merge($data, ['function' => $this->functPerson]));
            $this->valid($this->person);
            $this->personRepository->save($this->person);
            $this->em->commit();
            return $this->person;
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->functPerson = $this->functPersonRepository->getReference($_POST['function_id']);
            $this->person = $this->personRepository->getReference($id);
            $this->valid($this->person
                ->setName($_POST['name'])
                ->setFunction($this->functPerson)
            );
            $this->personRepository->save($this->person);
            $this->em->commit();
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }

    /**
     *
     */
    public function findActive()
    {
        // TODO: Implement findActive() method.
    }

    /**
     *
     */
    public function findInactive()
    {
        // TODO: Implement findInactive() method.
    }

    public function findByUniqueName(string $name)
    {
        try {
            return $this->personRepository->findByUniqueName($name);
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}