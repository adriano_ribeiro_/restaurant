<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 11:43
 */

namespace app\service\person;


interface PersonServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function findActive();

    public function findInactive();

    public function findByUniqueName(string $name);

}