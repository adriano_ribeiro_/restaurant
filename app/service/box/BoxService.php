<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:15
 */

namespace app\service\box;


use app\exception\ValidationException;
use app\factory\BoxFactory;
use app\model\Board;
use app\model\Box;
use app\model\Employee;
use app\model\Group;
use app\model\Product;
use app\model\Sale;
use app\repository\board\BoardRepository;
use app\repository\box\BoxRepository;
use app\repository\employee\EmployeeRepository;
use app\repository\group\GroupRepository;
use app\repository\product\ProductRepository;
use app\repository\sale\SaleRepository;
use app\service\board\BoardServiceInterface;
use app\service\group\GroupServiceInterface;
use app\service\sale\SaleServiceInterface;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class BoxService
 * @package app\service\box
 */
class BoxService extends Json implements BoxServiceInterface
{
    use ValidatorTrait;

    /** @var EntityManagerInterface */
    private $em;

    /** @var Board */
    private $board;

    /** @var Product */
    private $product;

    /** @var Sale */
    private $sale;

    /** @var Group */
    private $group;

    /** @var BoardServiceInterface */
    private $boardService;

    /** @var SaleServiceInterface */
    private $saleService;

    /** @var GroupServiceInterface */
    private $groupService;

    /** @var BoxRepository */
    private $boxRepository;

    /** @var BoardRepository */
    private $boardRepository;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var ProductRepository */
    private $productRepository;

    /** @var SaleRepository */
    private $saleRepository;

    /** @var GroupRepository */
    private $groupRepository;

    /**
     * @var array
     */
    private $boards;

    /**
     * BoxService constructor.
     * @param EntityManagerInterface $em
     * @param SaleServiceInterface $saleService
     * @param GroupServiceInterface $groupService
     * @param BoardServiceInterface $boardService
     */
    public function __construct(
        EntityManagerInterface $em,
        BoardServiceInterface $boardService,
        SaleServiceInterface $saleService,
        GroupServiceInterface $groupService
    )
    {

        $this->em = $em;
        $this->boardService = $boardService;
        $this->saleService = $saleService;
        $this->groupService = $groupService;

        $this->boxRepository = $this->em->getRepository(Box::class);
        $this->boardRepository = $this->em->getRepository(Board::class);
        $this->employeeRepository = $this->em->getRepository(Employee::class);
        $this->productRepository = $this->em->getRepository(Product::class);
        $this->saleRepository = $this->em->getRepository(Sale::class);
        $this->groupRepository = $this->em->getRepository(Group::class);
    }

    /**
     * @param array $data
     * @return Box
     */
    private function saveProductSale(array $data): Box
    {
        $this->product = $this->productRepository->getReference($data['products']); // id do produto
        $quantity = $data['quantity']; //quantidade do item
        $employee = $this->employeeRepository->getReference($data['waiters']); //id do garçon
        $box = BoxFactory::make(
            [
                'product' => $this->product,
                'quantity' => $quantity,
                'subtotal' => ($this->product->getPrice() * $quantity),
                'sale' => $this->sale,
                'waiter' => $employee
            ]
        );
        return $box;
    }


    /**
     * @param $data
     * @throws ValidationException
     */
    private function saveGroupBoards($data)
    {
        $this->board = $this->boardRepository->findBy([
            'id' => $data['boards']
        ]); // id da mesa

        /** @var Board $board */
        foreach ($this->board as $board) {
//            $this->boards[] = $this->boardRepository->getReference($board->getId());
            if ($board->isStatus() === true) {
                $this->boardService->updateStatusFalse($board->getId());
            }
        }

        if ($data['group']) {
            $this->group = $this->groupRepository->getReference($data['group']);
            $this->sale = $this->saleRepository->getReference($data['sale']);
            $this->valid($this->group
                ->setSale($this->sale)
                ->setBoard(new ArrayCollection($this->board))
            );
            $this->groupRepository->save($this->group);
        } else {
            $this->groupService->save(['board' => $this->board, 'sale' => $this->sale]);
        }
    }


    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();

            if ($data['sale']) {
                $this->sale = $this->saleRepository->getReference($data['sale']); // id da venda
                // verificar se existe o produto dentro da venda
                /** @var Box $productBox */ // how to update by foreign key
                $productBox = $this->boxRepository->findOneBy(['sale' => $data['sale'], 'product' => $data['products'], 'employee' => $data['waiters']]);
                //return $this->success($this->codeSuccess, $this->messageSuccess, $productBox->getSale()->getId());
                // se existir precisa somar a quantidade no banco mais a quantidade informada
                if ($productBox) {
                    // e multiplicar o valor unitário pela a nova quantidade
                    // para atualizar o valor total e a nova quantidade
                    $this->valid($productBox
                        ->setQuantity($productBox->getQuantity() + $data['quantity'])
                        ->setSubtotal($productBox->getQuantity() * $productBox->getProduct()->getPrice())
                    );
                    $this->boxRepository->save($productBox);
                    /** @var Sale $sale */
                    $sale = $this->saleRepository->getReference($data['sale']);
                    $this->valid($sale->setValueAll(($productBox->getProduct()->getPrice() * $data['quantity']) + $sale->getValueAll()));
                    $this->saleRepository->save($sale);
                    $this->saveGroupBoards($data);
                    $this->em->commit();
                    return $this->success($this->codeSuccess, $this->messageSuccess, $sale->getId());
                } else {
                    // se não existir add o product ao waiter que vendeu..
                    $box = $this->saveProductSale($data);
                    $this->valid($box);
                    $this->boxRepository->save($box);
                    /** @var Sale $sale */
                    $sale = $this->saleRepository->getReference($data['sale']);
                    $this->valid($sale->setValueAll($sale->getValueAll() + $box->getSubtotal()));
                    $this->saleRepository->save($sale);
                    $this->saveGroupBoards($data);
                    $this->em->commit();
                    return $this->success($this->codeSuccess, $this->messageSuccess, $sale->getId());
                }
            } else {
                /** salvo o sale com o valueAll 0.00 */
                $this->sale = $this->saleService->save($data);
                $box = $this->saveProductSale($data);
                $this->valid($box);
                $this->boxRepository->save($box);

                /** atualizo o valueAll de sale */
                /** @var Sale $sale */
                $sale = $this->saleRepository->getReference($this->sale->getId());
                $this->valid($sale->setValueAll($box->getSubtotal()));
                $this->saleRepository->save($sale);

                $this->saveGroupBoards($data);
                $groupSale = $this->groupService->groupSale($this->sale->getId());
                $this->em->commit();
                return $this->success($this->codeSuccess, $this->messageSuccess, $groupSale);
            }

        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

}