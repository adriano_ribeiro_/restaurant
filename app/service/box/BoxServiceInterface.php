<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:15
 */

namespace app\service\box;


interface BoxServiceInterface
{
    public function save(array $data);
}