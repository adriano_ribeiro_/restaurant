<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 11:42
 */

namespace app\service\employee;


use app\exception\ValidationException;
use app\factory\EmployeeFactory;
use app\model\Employee;
use app\model\FunctionPerson;
use app\model\Person;
use app\repository\employee\EmployeeRepository;
use app\repository\functionPerson\FunctionPersonRepository;
use app\repository\person\PersonRepository;
use app\service\person\PersonServiceInterface;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class EmployeeService
 * @package app\service\employee
 */
class EmployeeService extends Json implements EmployeeServiceInterface
{
    use ValidatorTrait;

    /** @var EntityManagerInterface */
    private $em;

    /** @var Employee */
    private $employee;

    /** @var FunctionPerson */
    private $function;

    /** @var EmployeeRepository */
    private $employeeRepository;

    /** @var PersonServiceInterface */
    private $personService;

    /** @var PersonRepository */
    private $personRepository;

    /** @var FunctionPersonRepository */
    private $functionPersonRepository;

    /**
     * EmployeeService constructor.
     * @param EntityManagerInterface $em
     * @param PersonServiceInterface $personService
     */
    public function __construct(EntityManagerInterface $em, PersonServiceInterface $personService)
    {
        $this->em = $em;
        $this->personService = $personService;

        $this->employeeRepository = $this->em->getRepository(Employee::class);
        $this->personRepository = $this->em->getRepository(Person::class);
        $this->functionPersonRepository = $this->em->getRepository(FunctionPerson::class);
    }

    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $person = $this->personService->save($data);
            $employee = EmployeeFactory::make(array_merge($data, ['person' => $person]));
            $this->valid($employee);
            $this->employeeRepository->save($employee);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $employee->getPerson()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            $this->employee = $this->employeeRepository->getReference($id);
            $this->function = $this->functionPersonRepository->getReference($_POST['function_id']);
            $this->valid(
                $this->employee
                    ->getPerson()->setName(trim($_POST['name']))
                    ->setFunction($this->function)
            );
            $this->employeeRepository->save($this->employee);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employee->getPerson()->getName());
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function updateStatus(int $id)
    {
        try {
            $this->employee = $this->employeeRepository->getReference($id);
            $this->valid($this->employee->getPerson()->setStatus($_POST['status']));
            $this->employeeRepository->save($this->employee);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employee->getPerson()->getName());
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function details(int $id)
    {
        try {
            $this->employee = $this->employeeRepository->getReference($id);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employee->jsonSerialize());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), null);
        }
    }

    /**
     * @return array
     */
    public function findActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employeeRepository->findActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }

    /**
     * @return array
     */
    public function findInactive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employeeRepository->findInactive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }

    /**
     * @return array
     */
    public function allWaiterActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->employeeRepository->allWaiterActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }
}