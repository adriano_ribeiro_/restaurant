<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 11:41
 */

namespace app\service\employee;


interface EmployeeServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function details(int $id);

    public function findActive();

    public function findInactive();

    public function allWaiterActive();
}