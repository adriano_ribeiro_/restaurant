<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 10:21
 */

namespace app\service\sale;


use app\exception\ValidationException;
use app\factory\SaleFactory;
use app\model\Sale;
use app\repository\sale\SaleRepository;
use app\service\board\BoardServiceInterface;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Class SaleService
 * @package app\service\sale
 */
class SaleService extends Json implements SaleServiceInterface
{
    use ValidatorTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Sale
     */
    private $sale;

    /**
     * @var BoardServiceInterface
     */
    private $boardService;

    /**
     * @var SaleRepository
     */
    private $saleRepository;

    /**
     * SaleService constructor.
     * @param EntityManagerInterface $em
     * @param BoardServiceInterface $boardService
     */
    public function __construct(EntityManagerInterface $em, BoardServiceInterface $boardService)
    {
        $this->em = $em;
        $this->boardService = $boardService;
        $this->saleRepository = $this->em->getRepository(Sale::class);
    }


    /**
     * @param array $data
     * @return Sale|array
     */
    public function save(array $data)
    {
        try {
            $this->sale = SaleFactory::make($data);
            $this->valid($this->sale);
            $this->saleRepository->save($this->sale);
            return $this->sale;
        } catch (ValidationException $e) {
            throw new $e;
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return Sale|array|object|null
     */
    public function update(int $id): Sale
    {
        try {
            $this->sale = $this->saleRepository->getReference($id);
            $this->valid($this->sale->setValueAll(0.00));
            $this->saleRepository->save($this->sale);
            return $this->sale;
        } catch (ValidationException $e) {
            throw new $e;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }


    /**
     * @param int $id
     * @return Sale|array|object|null
     */
    public function updateStatusTrue(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->sale = $this->saleRepository->getReference($id);
            $this->valid($this->sale->setStatus(true));
            $this->saleRepository->save($this->sale);
            $this->em->commit();
            return $this->sale;
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return Sale|array|object|null
     */
    public function updateStatusFalse(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->sale = $this->saleRepository->getReference($id);
            $this->valid($this->sale->setStatus(false));
            $this->saleRepository->save($this->sale);
            $this->em->commit();
            return $this->sale;
        } catch (ValidationException $e) {
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        // TODO: Implement delete() method.
    }


    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }


    /**
     * @param int $id
     * @return array
     */
    public function getValueBySale(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->getValueBySale($id));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function findActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->findActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @return array
     */
    public function findInactive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->findInactive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @return array
     */
    public function countSaleActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->countSaleActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function finishSale(int $id)
    {
        try {
            $this->em->beginTransaction();
            $boards = $this->saleRepository->finishSale($id);
            foreach ($boards as $board) {
                $this->boardService->updateStatusTrue((int)$board['id']);
            }

            $this->updateStatusFalse($id);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, 'Venda Finalizada com sucesso!');
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array|object|null
     */
    public function findById(int $id)
    {
        try {
//            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->findById($id));
            return $this->saleRepository->findById($id);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function detailsSale(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->detailsSale($id));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function getBoardsBySale(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->saleRepository->getBoardsBySale($id));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

}