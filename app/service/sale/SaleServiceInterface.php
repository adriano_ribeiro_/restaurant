<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 10:21
 */

namespace app\service\sale;


interface SaleServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function updateStatusTrue(int $id);

    public function updateStatusFalse(int $id);

    public function delete(int $id);

    public function all();

    public function findActive();

    public function findInactive();

    public function findById(int $id);

    public function detailsSale(int $id);

    public function getValueBySale(int $id);

    public function countSaleActive();

    public function finishSale(int $id);

    public function getBoardsBySale(int $id);

}