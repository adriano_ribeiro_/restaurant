<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:21
 */

namespace app\service\board;


interface BoardServiceInterface
{
    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function updateStatusTrue(int $id);

    public function updateStatusFalse(int $id);

    public function findActive();

    public function findInactive();

    public function details(int $id);
}