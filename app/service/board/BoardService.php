<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:20
 */

namespace app\service\board;


use app\exception\ValidationException;
use app\factory\BoardFactory;
use app\model\Board;
use app\repository\board\BoardRepository;
use app\traits\ValidatorTrait;
use app\util\Json;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class BoardService
 * @package app\service\board
 */
class BoardService extends Json implements BoardServiceInterface
{
    use ValidatorTrait;

    /** @var EntityManagerInterface */
    private $em;

    /** @var Board */
    private $board;

    /** @var BoardRepository */
    private $boardRepository;


    /**
     * BoardService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->boardRepository = $this->em->getRepository(Board::class);
    }

    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            if ($this->boardRepository->findByUniqueName($data['name'])) {
                $this->em->rollback();
                return $this->warning($this->codeWarning, $this->messageWarning, 'Mesa ' . $data['name'] . ' já existe.');
            }
            $board = BoardFactory::make($data);
            $this->valid($board);
            $this->boardRepository->save($board);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $board->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->board = $this->boardRepository->getReference($id);

            if ($this->board->getName() !== $_POST['name']) {
                if ($this->boardRepository->findByUniqueName($_POST['name'])) {
                    $this->em->rollback();
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Mesa ' . $_POST['name'] . ' já existe.');
                }
            }

            $this->valid($this->board->setName($_POST['name']));
            $this->boardRepository->save($this->board);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->board->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function updateStatus(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->board = $this->boardRepository->getReference($id);
            $this->valid($this->board->setStatus($_POST['status']));
            $this->boardRepository->save($this->board);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->board->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return Board|array|bool|object|null
     */
    public function updateStatusTrue(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->board = $this->boardRepository->getReference($id);
            $this->valid($this->board->setStatus(true));
            $this->boardRepository->save($this->board);
            $this->em->commit();
            return $this->board;
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return Board|array|bool|\Doctrine\Common\Proxy\Proxy|object|null
     */
    public function updateStatusFalse(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->board = $this->boardRepository->getReference($id);
            $this->valid($this->board->setStatus(false));
            $this->boardRepository->save($this->board);
            $this->em->commit();
            return $this->board;
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function findActive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->boardRepository->findActive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @return array
     */
    public function findInactive()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->boardRepository->findInactive());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function details(int $id)
    {
        try {
            $this->board = $this->boardRepository->getReference($id);
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->board->jsonSerialize());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $e->getMessage(), null);
        }
    }
}