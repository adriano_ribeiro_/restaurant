<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 15:58
 */

namespace app\service\user;


interface UserServiceInterface
{
    public function auth();

    public function logout();

    public function home();

    public function save(array $data);

    public function update(int $id);

    public function updateStatus(int $id);

    public function delete();

    public function all();

    public function active();
}