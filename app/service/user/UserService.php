<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 16:04
 */

namespace app\service\user;


use app\exception\ValidationException;
use app\factory\UserFactory;
use app\model\User;

use app\repository\user\UserRepository;
use app\service\person\PersonServiceInterface;
use app\traits\ValidatorTrait;
use app\util\Auth;
use app\util\Json;
use app\util\Password;
use Aura\Session\Session;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;


/**
 * Class UserService
 * @package app\service\user
 */
class UserService extends Json implements UserServiceInterface
{
    use ValidatorTrait;

    /** @var EntityManagerInterface */
    private $em;

    /** @var Session */
    protected $session;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /** @var PersonServiceInterface */
    private $personService;

    /** @var User */
    private $user;


    /**
     * BoardService constructor.
     * @param EntityManagerInterface $em
     * @param Session $session
     * @param PersonServiceInterface $personService
     */
    public function __construct(EntityManagerInterface $em, Session $session, PersonServiceInterface $personService)
    {
        $this->em = $em;
        $this->session = $session;
        $this->personService = $personService;
        $this->userRepository = $this->em->getRepository(User::class);
    }

    /**
     *
     */
    public function home()
    {
        try {
            $this->session;
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @return array|void
     */
    public function auth()
    {
        try {
            $this->em->beginTransaction();
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $auth = new Auth($this->session);
            $auth
                ->setEmail($_POST['email'])
                ->setPassword($_POST['password']);
            $this->valid($auth);

            /** @var User $user */
            $this->user = $this->userRepository->findOneBy([
                'email' => $_POST['email'],
            ]);
            // se existir o usuário vc pode comparar qualquer outro campo.
            if ($this->user) {
                if (Password::verify($_POST['password'], $this->user->getPassword())) {
                    $auth->generateUserSession($this->user);
                    return redirect('/');
                }
            }
            return $this->info($this->codeInfo, 'Usuário ou Senha incorretos', false);
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     *
     */
    public function logout()
    {
        $this->session->getSegment('Logged')->clear();
        return redirect('/usuario/login');
        //return $this->success($this->codeSuccess, $this->messageSuccess, false);
    }

    /**
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $person = $this->personService->save($data);
            $this->user = UserFactory::make(array_merge($data, ['person' => $person]));
            $this->valid($this->user);
            $this->userRepository->save($this->user);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->user->getPerson()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            $this->em->beginTransaction();
            $this->user = $this->userRepository->getReference($id);

            if ($this->user->getPerson()->getName() !== $_POST['name']) {
                if ($this->personService->findByUniqueName($_POST['name'])) {
                    $this->em->rollback();
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Usuário ' . $_POST['name'] . ' já existe.');
                }
            }

            if ($this->user->getEmail() !== $_POST['email']) {
                if ($this->userRepository->findByUniqueEmail($_POST['email'])) {
                    $this->em->rollback();
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Esse email ' . $_POST['email'] . ' já existe.');
                }
            }

            $this->personService->update($this->user->getPerson()->getId());

            $this->valid($this->user
                ->setEmail($_POST['email'])
            );

            $this->userRepository->save($this->user);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->user->getPerson()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }


    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }

    /**
     *
     */
    public function delete()
    {
        // TODO: Implement delete() method.
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }


    /**
     * @return array
     */
    public function active()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->userRepository->findActive());
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}