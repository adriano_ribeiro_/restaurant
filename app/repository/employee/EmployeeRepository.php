<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 10:11
 */

namespace app\repository\employee;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Doctrine\ORM\Query\Expr\Join;
use Throwable;


/**
 * Class EmployeeRepository
 * @package app\repository\employee
 */
class EmployeeRepository extends AbstractRepository implements InterfaceRepository, EmployeeRepositoryInterface
{

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e.id, p.name, f.name as function_id, p.status, e.createdAt, e.updatedAt')
                ->innerJoin('app\\model\\Person', 'p', Join::WITH, 'e.person = p.id')
                ->innerJoin('app\\model\\FunctionPerson', 'f', Join::WITH, 'p.function = f.id')
                ->where('p.status = :status')
                ->setParameter('status', true)
                ->groupBy('e.id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findInactive()
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e.id, p.name, f.name as function_id, p.status, e.createdAt, e.updatedAt')
                ->innerJoin('app\\model\\Person', 'p', Join::WITH, 'e.person = p.id')
                ->innerJoin('app\\model\\FunctionPerson', 'f', Join::WITH, 'p.function = f.id')
                ->where('p.status = :status')
                ->setParameter('status', false)
                ->groupBy('e.id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function allWaiterActive()
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e.id, p.name, f.name AS function_id, p.status, e.createdAt, e.updatedAt')
                ->innerJoin('app\\model\\Person', 'p', Join::WITH, 'e.person = p.id')
                ->innerJoin('app\\model\\FunctionPerson', 'f', Join::WITH, 'p.function = f.id')
                ->where('p.status = :status AND f.name = :name')
                ->setParameter('status', true)
                ->setParameter('name', 'garçom')
                ->groupBy('e.id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findNamePerson(int $id)
    {
        try {
            return $this->createQueryBuilder('e')
                ->select('e.id, p.name, f.name AS function_id, p.status, e.createdAt, e.updatedAt')
                ->innerJoin('app\\model\\Person', 'p', Join::WITH, 'e.person = p.id')
                ->innerJoin('app\\model\\FunctionPerson', 'f', Join::WITH, 'p.function = f.id')
                ->where('p.status = :status AND f.name = :name')
                ->setParameter('status', true)
                ->setParameter('name', 'garçom')
                ->groupBy('e.id')
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}