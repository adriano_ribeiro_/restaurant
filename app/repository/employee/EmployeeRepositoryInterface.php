<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 03/03/2019
 * Time: 08:46
 */

namespace app\repository\employee;


interface EmployeeRepositoryInterface
{
    public function allWaiterActive();

    public function findNamePerson(int $id);
}