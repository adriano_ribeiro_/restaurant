<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 10:32
 */

namespace app\repository;


interface InterfaceRepository
{
    public function findActive();

    public function findInactive();
}