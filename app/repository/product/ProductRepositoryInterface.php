<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 14:00
 */

namespace app\repository\product;


interface ProductRepositoryInterface
{
    public function productActiveSale();
}