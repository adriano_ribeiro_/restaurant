<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 14:00
 */

namespace app\repository\product;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Doctrine\DBAL\Types\ObjectType;
use Doctrine\ORM\Internal\Hydration\ObjectHydrator;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Throwable;


/**
 * Class ProductRepository
 * @package app\repository\product
 */
class ProductRepository extends AbstractRepository implements InterfaceRepository, ProductRepositoryInterface
{

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder('p')
                   ->select('p.id, p.name, p.description, p.price, p.createdAt, p.updatedAt')
                   ->where('p.status = :status')
                   ->setParameter('status', true)
                   ->getQuery()
                   ->getResult(Query::HYDRATE_OBJECT);
        } catch (Throwable $e) {
          throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findInactive()
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('p.id, p.name, p.description, p.price, p.createdAt, p.updatedAt')
                ->where('p.status = :status')
                ->setParameter('status', false)
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
          throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function productActiveSale()
    {
        try {
            return $this->createQueryBuilder('p')
                ->select('p.id, p.name, p.price')
                ->where('p.status = :status')
                ->setParameter('status', true)
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
          throw new $e;
        }
    }
}