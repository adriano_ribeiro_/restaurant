<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:26
 */

namespace app\repository\board;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Throwable;


/**
 * Class BoardRepository
 * @package app\repository\board
 */
class BoardRepository extends AbstractRepository implements InterfaceRepository, BoardRepositoryInterface
{

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b.id, b.name, b.status')
                ->where('b.status = :status AND b.name <> :name')
                ->setParameter('status', true)
                ->setParameter('name', 'admin')
                ->groupBy('b.name')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findInactive()
    {
        try {
            return $this->createQueryBuilder('b')
                ->select('b.id, b.name, b.status')
                ->where('b.status = :status AND b.name <> :name')
                ->setParameter('status', false)
                ->setParameter('name', 'admin')
                ->groupBy('b.name')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @param string $name
     * @return object|null
     */
    public function findByUniqueName(string $name)
    {
        try {
            return $this->findOneBy([
                'name' => $name
            ]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}