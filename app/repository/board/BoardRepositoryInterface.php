<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:25
 */

namespace app\repository\board;


/**
 * Interface BoardRepositoryInterface
 * @package app\repository\board
 */
interface BoardRepositoryInterface
{

    /**
     * @param string $name
     * @return mixed
     */
    public function findByUniqueName(string $name);
}