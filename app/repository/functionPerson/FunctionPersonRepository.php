<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 03/02/2019
 * Time: 14:25
 */

namespace app\repository\functionPerson;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Throwable;

/**
 * Class FunctionPersonRepository
 * @package app\repository\functionPerson
 */
class FunctionPersonRepository extends AbstractRepository implements InterfaceRepository, FunctionPersonRepositoryInterface
{

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder('f')
                ->select('f.id, f.name, f.status, f.createdAt, f.updatedAt')
                ->where('f.status = :status AND f.name <> :name')
                ->setParameter('status', true)
                ->setParameter('name', 'admin')
                ->groupBy('f.id')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     *
     */
    public function findInactive()
    {
        // TODO: Implement findInactive() method.
    }
}