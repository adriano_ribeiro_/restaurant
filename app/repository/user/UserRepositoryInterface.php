<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 16:55
 */

namespace app\repository\user;


interface UserRepositoryInterface
{
    public function all();


    public function findByUniqueEmail(string $email);
}