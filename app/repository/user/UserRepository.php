<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 11:26
 */

namespace app\repository\user;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Doctrine\ORM\Query\Expr\Join;
use Throwable;

/**
 * Class UserRepository
 * @package app\repository\user
 */
class UserRepository extends AbstractRepository implements InterfaceRepository, UserRepositoryInterface
{

    /**
     * @return mixed
     */
    public function all()
    {
        try {
            //TODO: assim tbm dá certo.
            /** return $this->getEntityManager()->createQueryBuilder()
             * ->select('u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
             * ->from('app\models\User', 'u')
             * ->innerJoin('app\models\FunctionPersonRepository', 'f')
             * ->orWhere('u.function = f.id')
             * ->getQuery()
             * ->getResult();  */

            return $this->createQueryBuilder('u')
                ->select('u.id, u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
                ->innerJoin('app\\models\\FunctionUser', 'f', Join::WITH, 'u.function = f.id')
                ->groupBy('u.id')
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
           throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder("u")
                ->select('u.id, p.name, u.email, f.name as function_id, u.createdAt, u.updatedAt')
                ->innerJoin('u.person', 'p', 'WITH', 'u.person = p.id')
                ->innerJoin('p.function', 'f', 'WITH', 'f.id = p.function')
                ->where('p.status = :status AND f.name <> :name')
                ->setParameter('status', true)
                ->setParameter('name', 'admin')
                ->groupBy('p.name')
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
           throw new $e;
        }
    }


    /**
     * @return mixed
     */
    public function findInactive()
    {
        try {
            return $this->createQueryBuilder("u")
                ->select('u.id, u.name, u.email, f.name as function_id, u.status, u.createdAt, u.updatedAt')
                ->innerJoin('u.function', 'f', Join::WITH, 'u.status = :status AND f.name <> :name')
                ->setParameter('status', false)
                ->setParameter('name', 'admin')
                ->groupBy('u.name')
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
           throw new $e;
        }
    }


    public function findByUniqueEmail(string $email)
    {
        try {
            return $this->findOneBy(['email' => $email]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}