<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:21
 */

namespace app\repository\group;


/**
 * Interface GroupRepositoryInterface
 * @package app\repository\group
 */
interface GroupRepositoryInterface
{
    /**
     * @return mixed
     */
    public function saleActive();

    /**
     * @param int $id
     * @return mixed
     */
    public function groupSale(int $id);
}