<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:21
 */

namespace app\repository\group;


use app\repository\AbstractRepository;
use Throwable;


/**
 * Class GroupRepository
 * @package app\repository\group
 */
class GroupRepository extends AbstractRepository implements GroupRepositoryInterface
{

    /**
     * @return mixed
     */
    public function saleActive()
    {
        try {
            return $this->createQueryBuilder('g')
                ->select('s.id, b.name, s.valueAll')
                ->innerJoin('g.boards', 'b', 'WITH', 'b.id = g.boards')
                ->innerJoin('g.sale', 's', 'WITH', 's.id = g.sale')
                ->where('s.status = :status')
                ->setParameter('status', true)
                ->orderBy('s.id', 'asc')
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function groupSale(int $id)
    {
        try {
            return $this->createQueryBuilder('g')
                ->select('g.id AS group, s.id AS sale')
                ->innerJoin('g.sale', 's', 'WITH', 's.id = g.sale')
                ->where('s.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}