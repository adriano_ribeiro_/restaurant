<?php


namespace app\repository\person;


interface PersonRepositoryInterface
{
    public function findByUniqueName(string $name);
}