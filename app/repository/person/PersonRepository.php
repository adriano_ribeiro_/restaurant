<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 10:12
 */

namespace app\repository\person;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Throwable;

/**
 * Class PersonRepository
 * @package app\repository\person
 */
class PersonRepository extends AbstractRepository implements InterfaceRepository
{

    /**
     *
     */
    public function findActive()
    {
        // TODO: Implement findActive() method.
    }

    /**
     *
     */
    public function findInactive()
    {
        // TODO: Implement findInactive() method.
    }


    /**
     * @param string $name
     * @return object|null
     */
    public function findByUniqueName(string $name)
    {
        try {
            return $this->findOneBy(['name' => $name]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }


}