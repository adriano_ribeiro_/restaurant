<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 10:15
 */

namespace app\repository\sale;


interface SaleRepositoryInterface
{
    public function findById(int $id);

    public function detailsSale(int $id);

    public function getValueBySale(int $id);

    public function countSaleActive();

    public function finishSale(int $id);

    public function getBoardsBySale(int $id);

}