<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 10:20
 */

namespace app\repository\sale;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Throwable;

/**
 * Class SaleRepository
 * @package app\repository\sale
 */
class SaleRepository extends AbstractRepository implements InterfaceRepository, SaleRepositoryInterface
{

    /**
     * @param int $id
     * @return object|null
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findActive()
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.id, b.name AS boards, s.valueAll')
                ->innerJoin('app\model\Group', 'g', 'WITH', 's.id = g.sale')
                ->innerJoin('app\model\Board', 'b', 'WITH', 'b.id = g.board')
                ->where('s.status = :status')
                ->setParameter('status', true)
                ->orderBy('s.id', 'asc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @return mixed
     */
    public function findInactive()
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.id, b.name, s.valueAll')
                ->innerJoin('app\\model\\Group', 'g', 'WITH', 's.id = g.sale')
                ->innerJoin('app\\model\\Board', 'b', 'WITH', 'b.id = g.board')
                ->where('s.status = :status')
                ->setParameter('status', false)
                ->orderBy('s.id', 'asc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function detailsSale(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.id AS sale, p.name AS product, p.price AS priceUnit, SUM(b.quantity) AS quantity, SUM(b.subtotal) AS subtotal')
                ->innerJoin('app\\model\\Box', 'b', 'WITH', 'b.sale = s.id')
                ->innerJoin('app\\model\\Product', 'p', 'WITH', 'p.id = b.product')
                ->where('s.status = :status AND b.sale = :sale')
                ->setParameter('status', true)
                ->setParameter('sale', $id)
                ->groupBy('s.id, p.name, p.price, b.quantity, b.subtotal')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function detailsSaleByWaiter(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('b.id, s.id AS sale, p.id AS product_id, p.name AS product, p.price AS priceUni, b.quantity, b.subtotal, pe.name AS waiter')
                ->innerJoin('app\\model\\Box', 'b', 'WITH', 'b.sale = s.id')
                ->innerJoin('app\\model\\Product', 'p', 'WITH', 'p.id = b.product')
                ->innerJoin('app\\model\\Employee', 'e', 'WITH', 'e.id = b.employee')
                ->innerJoin('app\\model\\Person', 'pe', 'WITH', 'pe.id = e.person')
                ->where('s.status = :status AND b.sale = :sale')
                ->setParameter('status', true)
                ->setParameter('sale', $id)
                ->orderBy('b.id', 'asc')
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function getValueBySale(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.id, s.valueAll')
                ->where('s.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @return mixed
     */
    public function countSaleActive()
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('count(s)')
                ->where('s.status = :status')
                ->setParameter('status', true)
                ->getQuery()
                ->getSingleScalarResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function finishSale(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('b.id')
                ->innerJoin('app\\model\\Group', 'g', 'WITH', 's.id = g.sale')
                ->innerJoin('app\\model\\Board', 'b', 'WITH', 'b.id = g.board')
                ->where('s.status = :status AND s.id = :id')
                ->setParameter('status', true)
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function getBoardsBySale(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('b.id, b.name')
                ->innerJoin('app\\model\\Group', 'g', 'WITH', 's.id = g.sale')
                ->innerJoin('app\\model\\Board', 'b', 'WITH', 'b.id = g.board')
                ->where('g.sale = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getResult();
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}