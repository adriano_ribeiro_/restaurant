<?php

namespace app\repository;


use Doctrine\Common\Proxy\Proxy;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\UnitOfWork;


/**
 * Class AbstractRepository
 * @package app\repository
 */
abstract class AbstractRepository extends EntityRepository
{

    /**
     * @param int $id
     * @param null $class
     * @return bool|Proxy|object|null
     */
    public function getReference(int $id, $class = null)
    {
        try {
            if (!$class) {
                $class = $this->getClassName();
            }
            return $this->getEntityManager()->getReference($class, $id);
        } catch (ORMException $e) {
            throw new $e;
        }
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        try {
            if ($this->getEntityManager()->getUnitOfWork()->getEntityState($entity) == UnitOfWork::STATE_NEW) {
                $this->getEntityManager()->persist($entity);
            }
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new $e;
        }
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        try {
            $entity = $this->getReference($id);
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new $e;
        }
    }
}