<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 16:20
 */

namespace app\repository\box;


use app\repository\AbstractRepository;
use app\repository\InterfaceRepository;
use Throwable;


/**
 * Class BoxRepository
 * @package app\repository\box
 */
class BoxRepository extends AbstractRepository implements InterfaceRepository, BoxRepositoryInterface
{

    public function findActive()
    {
        // TODO: Implement findActive() method.
    }

    public function findInactive()
    {
        // TODO: Implement findInactive() method.
    }

}