<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:51
 */

namespace app\model;


use app\traits\FieldsGeneric;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\user\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends Model
{
    use FieldsGeneric;

    /**
     * @var Person
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="Usuário obrigatório")
     */
    private $person;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo email obrigatório")
     *
     *@Assert\Email(message="email inválido")
     */
    private $email;

    /**
     * @var string $password
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Campo senha obrigatório")
     *
     * @Assert\Length(min="4", minMessage="Campo senha deve ter no mínimo 4 caracteres")
     *
     */
    private $password;

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return User
     */
    public function setPerson(Person $person): User
    {
        $this->person = $person;
        return $this;
    }


    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getPerson()->getName(),
            'email' => $this->getEmail(),
            'function_id' => $this->getPerson()->getFunction()->getName(),
            'status'=> $this->getPerson()->isStatus(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        );
    }
}