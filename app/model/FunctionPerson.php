<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:54
 */

namespace app\model;


use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\functionPerson\FunctionPersonRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="functions")
 */
class FunctionPerson extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotNull
     */
    private $status;


    /**
     * @var Person
     *
     * @ORM\OneToMany(targetEntity="app\model\Person", cascade={"persist"}, mappedBy="function")
     */
    private $person;

    public function __construct()
    {
        $this->person = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return FunctionPerson
     */
    public function setName(string $name): FunctionPerson
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return FunctionPerson
     */
    public function setStatus(bool $status): FunctionPerson
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param Person addPerson
     * @return FunctionPerson
     */
    public function addPerson(Person $person)
    {
        //$this->users[] = $user;
        $this->person->add($person);
        $person->setFunction($this);
        return $this;
    }

    /**
     * @return Person
     */
    public function getPerson():Person
    {
        return $this->person->toArray();
    }
}