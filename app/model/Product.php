<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\model;


use app\traits\FieldsGeneric;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\product\ProductRepository")
 * @ORM\Table(name="products")
 * @ORM\HasLifecycleCallbacks()
 */
class Product extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", unique=true, length=100, nullable=false)
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=200, nullable=false)
     *
     * @Assert\NotBlank(message="Campo descrição obrigatório")
     */
    private $description;

    /**
     * @var float $price
     *
     * @ORM\Column(type="decimal", precision=8, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo preço obrigatório")
     */
    private $price;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotNull
     */
    private $status;

    /**
     * @var Box
     *
     * @ORM\OneToMany(targetEntity="app\model\Box", cascade={"persist"}, mappedBy="product")
     */
    private $boxes;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Product
     */
    public function setName(string $name): Product
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Product
     */
    public function setPrice(float $price): Product
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Product
     */
    public function setStatus(bool $status): Product
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Box
     */
    public function getBoxes(): Box
    {
        return $this->boxes;
    }

    /**
     * @param Box $boxes
     * @return Product
     */
    public function setBoxes(Box $boxes): Product
    {
        $this->boxes = $boxes;
        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'price' => $this->getPrice(),
            'status'=> $this->isStatus(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        );
    }

    public function namePrice()
    {
        return array(
            "id" => $this->getId(),
            "text" => $this->getName() . ' - ' . 'R$ ' . number_format($this->getPrice(), 2, ',', '.')
        );
    }
}