<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 27/10/2018
 * Time: 14:24
 */

namespace app\model;

use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\board\BoardRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="boards")
 */
class Board extends Model
{
    use FieldsGeneric;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", unique=true, nullable=false, length=10)
     *
     * @Assert\NotBlank(message="Campo N° obrigatório")
     */
    private $name;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", nullable=false, length=1)
     *
     * @Assert\NotNull
     */
    private $status;


    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="app\model\Group", mappedBy="board")
     */
    private $group;

    public function __construct()
    {
        $this->group = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Board
     */
    public function setName(string $name): Board
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Board
     */
    public function setStatus(bool $status): Board
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getGroup(): ?Collection
    {
        return $this->group;
    }

    /**
     * @param Collection $group
     * @return Board
     */
    public function setGroup(Collection $group): Board
    {
        $this->group = $group;
        return $this;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'status'=> $this->isStatus(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        );
    }
}