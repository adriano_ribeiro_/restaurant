<?php
/**
 * Created by PhpStorm.
 * Person: adriano
 * Date: 02/03/2019
 * Time: 09:38
 */

namespace app\model;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\person\PersonRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="people")
 */
class Person extends Model
{
    /**
     * @var string $name
     *
     * @ORM\Column(type="string", length=100, unique=true, nullable=false)
     *
     * @Assert\Type(
     *     type="string",
     *     message="Campo nome tem que ser do tipo caracter"
     * )
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var FunctionPerson
     *
     * @ORM\ManyToOne(targetEntity="app\model\FunctionPerson", cascade={"persist"}, inversedBy="person")
     * @ORM\JoinColumn(name="function_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo função obrigatório")
     */
    private $function;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotNull
     */
    private $status;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Person
     */
    public function setName(string $name): Person
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return FunctionPerson
     */
    public function getFunction(): FunctionPerson
    {
        return $this->function;
    }

    /**
     * @param FunctionPerson $function
     * @return Person
     */
    public function setFunction(FunctionPerson $function): Person
    {
        $this->function = $function;
        return $this;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Person
     */
    public function setStatus(bool $status): Person
    {
        $this->status = $status;
        return $this;
    }
}