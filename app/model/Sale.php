<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 10:14
 */

namespace app\model;

use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\sale\SaleRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="sales")
 */
class Sale extends Model
{
    use FieldsGeneric;

    /**
     * @var float $valueAll
     *
     * @ORM\Column(type="decimal", name="value_all", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotNull(message="Campo Valor Total obrigatório")
     */
    private $valueAll;

    /**
     * @var boolean $status
     *
     * @ORM\Column(type="boolean", length=1, nullable=false)
     *
     * @Assert\NotNull
     */
    private $status;

    /**
     * @var Box
     *
     * @ORM\OneToMany(targetEntity="app\model\Box", cascade={"persist"}, mappedBy="sale")
     */
    private $box;

    /**
     * @var Group
     *
     * @ORM\OneToMany(targetEntity="app\model\Group", cascade={"persist"}, mappedBy="sale")
     */
    private $group;

    public function __construct()
    {
        $this->box = new ArrayCollection();
        $this->group = new ArrayCollection();
    }


    /**
     * @return float|null
     */
    public function getValueAll(): ?float
    {
        return $this->valueAll;
    }

    /**
     * @param float $valueAll
     * @return Sale
     */
    public function setValueAll(float $valueAll): Sale
    {
        $this->valueAll = $valueAll;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isStatus(): ?bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return Sale
     */
    public function setStatus(bool $status): Sale
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param Box $box
     * @return Sale|null
     */
    public function setBox(Box $box): ?Sale
    {
        //$this->users[] = $user;
        $this->box->add($box);
        $box->setSale($this);
        return $this;
    }

    /**
     * @return Box|null
     */
    public function getBox(): ?Box
    {
        return $this->box->toArray();
    }

    /**
     * @return Collection|null
     */
    public function getGroup(): ?Collection
    {
        return $this->group;
    }

    /**
     * @param Collection $group
     * @return Sale
     */
    public function setGroup(Collection $group): Sale
    {
        $this->group = $group;
        return $this;
    }


    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'valueAll' => $this->getValueAll()
        );
    }
}