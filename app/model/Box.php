<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\model;

use app\traits\FieldsGeneric;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="app\repository\box\BoxRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="boxes")
 */
class Box extends Model
{
    use FieldsGeneric;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="app\model\Product", cascade={"persist"}, inversedBy="boxes")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo produto obrigatório")
     */
    private $product;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(type="integer", name="quantity", nullable=false)
     *
     * @Assert\NotBlank(message="Campo quantidade obrigatório")
     */
    private $quantity;

    /**
     * @var float $subtotal
     *
     * @ORM\Column(type="decimal", name="subtotal", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotNull(message="Campo subtotal obrigatório")
     */
    private $subtotal;

    /**
     * @var Employee
     *
     * @ORM\ManyToOne(targetEntity="app\model\Employee", cascade={"persist"}, inversedBy="boxes")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo garçom obrigatório")
     */
    private $employee;

    /**
     * @var Sale
     *
     * @ORM\ManyToOne(targetEntity="app\model\Sale", cascade={"persist"}, inversedBy="box")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo venda obrigatório")
     */
    private $sale;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return Box
     */
    public function setProduct(Product $product): Box
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return Box
     */
    public function setQuantity(int $quantity): Box
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getSubtotal(): float
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     * @return Box
     */
    public function setSubtotal(float $subtotal): Box
    {
        $this->subtotal = $subtotal;
        return $this;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }

    /**
     * @param Employee $employee
     * @return Box
     */
    public function setEmployee(Employee $employee): Box
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return Sale
     */
    public function getSale(): Sale
    {
        return $this->sale;
    }

    /**
     * @param Sale $sale
     * @return Box
     */
    public function setSale(Sale $sale): Box
    {
        $this->sale = $sale;
        return $this;
    }
}