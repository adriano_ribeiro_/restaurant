<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 17/11/2018
 * Time: 07:52
 */

namespace app\model;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="app\repository\group\GroupRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="groups")
 */
class Group extends Model
{

    /**
     * @var Sale
     *
     * @ORM\ManyToOne(targetEntity="app\model\Sale", cascade={"persist"}, inversedBy="group")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo venda obrigatório")
     */
    private $sale;


    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="app\model\Board", inversedBy="group")
     * @ORM\JoinTable(name="groups_boards",
     * joinColumns={
     *     @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="board_id", referencedColumnName="id")
     *   }
     * )
     */
    private $board;



    public function __construct()
    {
        $this->board = new ArrayCollection();
    }


    /**
     * @return Sale|null
     */
    public function getSale(): ?Sale
    {
        return $this->sale;
    }

    /**
     * @param Sale $sale
     * @return Group
     */
    public function setSale(Sale $sale): Group
    {
        $this->sale = $sale;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getBoard(): ?Collection
    {
        return $this->board;
    }

    /**
     * @param Collection $board
     * @return Group
     */
    public function setBoard(Collection $board): Group
    {
        $this->board = $board;
        return $this;
    }


    //    /**
//     * @param Board addBoards
//     * @return Group
//     */
//    public function addBoards($board)
//    {
//        //$this->users[] = $user;
//        $this->boards->add($board);
//        $board->setGroup($this);
//        return $this;
//    }

//    public function addBoards($boards)
//    {
//        /** @var Board $board */
//        foreach ($boards as $board) {
//            $this->boards->add($board);
//            $board->setGroup($this);
//        }
//        return $this;
//    }

}