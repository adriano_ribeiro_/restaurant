<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 09:30
 */

namespace app\model;


use app\traits\FieldsGeneric;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="app\repository\employee\EmployeeRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="employees")
 */
class Employee extends Model
{
    use FieldsGeneric;

    /**
     * @var Person
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="Funcionário obrigatório")
     */
    private $person;

    /**
     * @var Box
     *
     * @ORM\OneToMany(targetEntity="app\model\Box", cascade={"persist"}, mappedBy="employee")
     */
    private $boxes;

    public function __construct()
    {
        $this->boxes = new ArrayCollection();
    }

    /**
     * @return Person
     */
    public function getPerson(): Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Employee
     */
    public function setPerson(Person $person): Employee
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Employee
     */
    public function getBoxes(): Employee
    {
        return $this->boxes->toArray();
    }

    /**
     * @param Box $boxes
     * @return Employee
     */
    public function setBoxes(Box $boxes): Employee
    {
        //$this->boxes[] = $boxes;
        $this->boxes->add($boxes);
        $boxes->setEmployee($this);
        return $this;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'name' => $this->getPerson()->getName(),
            'function_id' => $this->getPerson()->getFunction()->getName(),
            'status' => $this->getPerson()->isStatus(),
            'createdAt' => $this->getCreatedAt(),
            'updatedAt' => $this->getUpdatedAt()
        );
    }
}