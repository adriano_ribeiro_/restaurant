<?php
/**
 * Created by PhpStorm.
 * UserModel: adriano
 * Date: 26/07/2018
 * Time: 17:24
 */

namespace app\util;


/**
 * Class Password
 * @package app\util
 */
class Password
{

    /**
     * @param $password
     * @return bool|string
     */
    public static function hash($password)
    {
        $options = [
            'cost' => 12
        ];

        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    public static function verify($password, $hash)
    {
        return password_verify($password, $hash);
    }
}