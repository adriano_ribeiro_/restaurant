<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 13:45
 */

namespace app\util;


/**
 * Class Json
 * @package app\util
 */
abstract class Json
{
    /**
     * @var bool
     */
    protected $codeSuccess = true;

    /**
     * @var bool
     */
    protected $codeError = false;

    /**
     * @var int
     */
    protected $codeWarning = 2;

    /**
     * @var int
     */
    protected $codeInfo = 1;

    /**
     * @var string
     */
    protected $messageSuccess = 'SUCESSO ';

    /**
     * @var string
     */
    protected $messageError = 'ERRO ';

    /**
     * @var string
     */
    protected $messageInfo = 'INFORMAÇÃO ';

    /**
     * @var string
     */
    protected $messageWarning = 'ADVERTÊNCIA ';

    /**
     * @var string
     */
    protected $validation = 'VALIDAÇÃO ';


    /**
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    public function success ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    public function error ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    public function info ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    public function warning ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return array
     */
    private function toJson($code, $message, $data)
    {
        header('Content-Type: application/json; charset=UTF-8');

        $json = [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];

        echo json_encode($json);
        return $json;
    }
}