<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 13:27
 */

namespace app\factory;


use app\model\Box;


class BoxFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $box = new Box();
        return $box
            ->setProduct($data['product'])
            ->setQuantity(trim((int)$data['quantity']))
            ->setSubtotal(trim((float)$data['subtotal']))
            ->setSale($data['sale'])
            ->setEmployee($data['waiter'])
        ;
    }
}