<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 14:01
 */

namespace app\factory;


use app\model\Product;

class ProductFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $product = new Product();
        return $product
            ->setName(trim($data['name']))
            ->setDescription(trim($data['description']))
            ->setPrice((float)trim($data['price']))
            ->setStatus(true)
        ;
    }
}