<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 10:53
 */

namespace app\factory;


use app\model\Person;

class PersonFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $person = new Person();
        return $person
            ->setName(trim($data['name']))
            ->setFunction($data['function'])
            ->setStatus(true)
        ;
    }
}