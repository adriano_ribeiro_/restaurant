<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 11:10
 */

namespace app\factory;


interface FactoryInterface
{
    public static function make(array $data);
}