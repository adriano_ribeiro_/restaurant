<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 30/01/2019
 * Time: 09:03
 */

namespace app\factory;


use app\model\User;
use app\util\Password;

class UserFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $user = new User();
        return $user
            ->setPerson($data['person'])
            ->setEmail(trim($data['email']))
            ->setPassword(Password::hash(trim($data['password'])))
        ;
    }
}