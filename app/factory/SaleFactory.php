<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 12:47
 */

namespace app\factory;


use app\model\Sale;

class SaleFactory implements FactoryInterface
{

    public static function make(array $data)
    {
       $sale = new Sale();
       return $sale
           ->setValueAll( 0.00)
           ->setStatus(true)
       ;
    }
}