<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 30/01/2019
 * Time: 09:41
 */

namespace app\factory;


use app\model\FunctionPerson;


class FunctionPersonFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $functUser = new FunctionPerson();
        return $functUser
            ->setName(trim($data['name']))
            ->setStatus(true)
        ;
    }
}