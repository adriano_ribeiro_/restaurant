<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 28/01/2019
 * Time: 11:10
 */

namespace app\factory;


use app\model\Board;


class BoardFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $board = new Board();
        return $board
            ->setName(trim($data['name']))
            ->setStatus(true)
        ;
    }
}