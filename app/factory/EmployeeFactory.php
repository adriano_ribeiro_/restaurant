<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 10:54
 */

namespace app\factory;


use app\model\Employee;

class EmployeeFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $employee = new Employee();
        return $employee
            ->setPerson($data['person'])
        ;

    }
}