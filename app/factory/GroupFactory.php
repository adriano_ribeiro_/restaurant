<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/02/2019
 * Time: 15:41
 */

namespace app\factory;


use app\model\Group;
use Doctrine\Common\Collections\ArrayCollection;


class GroupFactory implements FactoryInterface
{

    public static function make(array $data)
    {
        $group = new Group();
        return $group
            ->setSale($data['sale'])
            ->setBoard(new ArrayCollection($data['board']))
        ;
    }
}