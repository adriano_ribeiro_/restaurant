<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 23/01/2019
 * Time: 14:13
 */

namespace app\provider;


use app\util\TwigFunctions;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;


abstract class View
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;


    public function __construct()
    {
        $loader = new \Twig_Loader_Filesystem(base_path('resources/views'));
        $twig = new \Twig_Environment($loader);

        $twigFunctions = new \Twig_SimpleFunction(\TwigFunctions::class, function ($method, $params = []) {
            return TwigFunctions::$method($params);
        });
        $twig->addFunction($twigFunctions);

        $this->twig = $twig;
    }


    public function render(string $view, array $data = []): string
    {
        try {
            return $this->twig->render($view, $data);
        } catch (LoaderError $e) {
            $e->getMessage();
        } catch (RuntimeError $e) {
            $e->getMessage();
        } catch (SyntaxError $e) {
            $e->getMessage();
        }
    }
}