<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 15/04/2019
 * Time: 17:53
 */

namespace app\controller;


use app\service\group\GroupServiceInterface;
use Throwable;

/**
 * Class GroupController
 * @package app\controller
 */
class GroupController
{
    /**
     * @var GroupServiceInterface
     */
    private $service;


    /**
     * BoxController constructor.
     * @param GroupServiceInterface $service
     */
    public function __construct(GroupServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function saleActive() {
        try {
            return $this->service->saleActive();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}