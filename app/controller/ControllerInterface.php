<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 30/01/2019
 * Time: 09:23
 */

namespace app\controller;


interface ControllerInterface
{
    public function store();

    public function update(int $id);

    public function updateStatus(int $id);

    public function active();

    public function inactive();

    public function all();

    public function find();
}