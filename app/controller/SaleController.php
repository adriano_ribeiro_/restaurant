<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/04/2019
 * Time: 17:01
 */

namespace app\controller;


use app\service\sale\SaleServiceInterface;
use Throwable;


/**
 * Class SaleController
 * @package app\controller
 */
class SaleController implements SaleControllerInterface
{
    /**
     * @var SaleServiceInterface
     */
    private $service;


    /**
     * BoardView constructor.
     * @param SaleServiceInterface $service
     */
    public function __construct(SaleServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getValueBySale(int $id)
    {
        try {
            return $this->service->getValueBySale($id);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function saleActive()
    {
        try {
            return $this->service->findActive();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @return mixed
     */
    public function countSaleActive()
    {
        try {
            return $this->service->countSaleActive();
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function finishSale(int $id)
    {
        try {
            return $this->service->finishSale($id);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id)
    {
        try {
            return $this->service->findById($id);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function detailsSale(int $id)
    {
        try {
            return $this->service->detailsSale($id);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getBoardsBySale(int $id)
    {
        try {
            return $this->service->getBoardsBySale($id);
        } catch (Throwable $e) {
            $e->getMessage();
        }
    }
}