<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 01/02/2019
 * Time: 14:11
 */

namespace app\controller;


use app\service\product\ProductServiceInterface;
use Throwable;


/**
 * Class ProductController
 * @package app\controller
 */
class ProductController implements ControllerInterface
{
    /**
     * @var ProductServiceInterface
     */
    private $service;


    /**
     * ProductController constructor.
     * @param ProductServiceInterface $service
     */
    public function __construct(ProductServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function store()
    {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function update(int $id)
    {
        try {
            return $this->service->update($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function updateStatus(int $id)
    {
        try {
            return $this->service->updateStatus($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function active()
    {
        try {
            return $this->service->findActive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function inactive()
    {
        try {
            return $this->service->findInactive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function productActiveSale()
    {
        try {
            return $this->service->productActiveSale();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }

    /**
     * @param int $id
     * @return string
     */
    public function details(int $id)
    {
        try {
            return $this->service->details($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }
}