<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 03/02/2019
 * Time: 14:44
 */

namespace app\controller;


use app\service\functionPerson\FunctionPersonServiceInterface;
use Throwable;


/**
 * Class FunctionPersonController
 * @package app\controller
 */
class FunctionPersonController implements ControllerInterface
{
    /** @var FunctionPersonServiceInterface */
    private $service;

    /**
     * FunctionPersonController constructor.
     * @param FunctionPersonServiceInterface $service
     */
    public function __construct(FunctionPersonServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function store(){
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     */
    public function update(int $id)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }

    /**
     * @return string
     */
    public function active(){
        try {
            return $this->service->findActive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     *
     */
    public function inactive()
    {
        // TODO: Implement inactive() method.
    }
}