<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 14:12
 */

namespace app\controller;


use app\exception\ValidationException;
use app\service\board\BoardServiceInterface;
use app\util\Json;
use app\util\Request;
use Throwable;


/**
 * Class BoardController
 * @package app\controller
 */
class BoardController extends Json implements ControllerInterface
{

    /**
     * @var BoardServiceInterface
     */
    private $service;


    /**
     * BoardView constructor.
     * @param BoardServiceInterface $service
     */
    public function __construct(BoardServiceInterface $service)
    {
        $this->service = $service;
    }


    /**
     * @return array
     */
    public function store()
    {
        try {
            if (Request::request('post')) {
                return $this->service->save($_POST);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            if (Request::request('post')) {
                return $this->service->update($id);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function updateStatus(int $id)
    {
        try {
            if (Request::request('post')) {
                return $this->service->updateStatus($id);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @param int $id
     * @return array
     */
    public function updateStatusTrue(int $id)
    {
        try {
            if (Request::request('post')) {
                return $this->service->updateStatusTrue($id);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @return array
     */
    public function active()
    {
        try {
            if (Request::request('get')) {
                return $this->service->findActive();
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @return array
     */
    public function inactive()
    {
        try {
            if (Request::request('get')) {
                return $this->service->findInactive();
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }


    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }


    /**
     * @param int $id
     * @return array
     */
    public function details(int $id)
    {
        try {
            if (Request::request('get')) {
                return $this->service->details($id);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }
}