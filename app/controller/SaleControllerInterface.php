<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/04/2019
 * Time: 17:04
 */

namespace app\controller;


interface SaleControllerInterface
{
    public function findById(int $id);

    public function detailsSale(int $id);

    public function getValueBySale(int $id);

    public function countSaleActive();

    public function finishSale(int $id);

    public function getBoardsBySale(int $id);

}