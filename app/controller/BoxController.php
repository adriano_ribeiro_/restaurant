<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 05/02/2019
 * Time: 18:15
 */

namespace app\controller;


use app\service\box\BoxServiceInterface;
use Throwable;


/**
 * Class BoxController
 * @package app\controller
 */
class BoxController implements ControllerInterface
{
    /**
     * @var BoxServiceInterface
     */
    private $service;


    /**
     * BoxController constructor.
     * @param BoxServiceInterface $service
     */
    public function __construct(BoxServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function store()
    {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     */
    public function update(int $id)
    {
        // TODO: Implement update() method.
    }

    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }

    /**
     *
     */
    public function active()
    {
        // TODO: Implement active() method.
    }

    /**
     *
     */
    public function inactive()
    {
        // TODO: Implement inactive() method.
    }
}