<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 15:40
 */

namespace app\controller;


use app\service\employee\EmployeeServiceInterface;
use Throwable;

/**
 * Class EmployeeController
 * @package app\controller
 */
class EmployeeController implements ControllerInterface
{
    /**
     * @var EmployeeServiceInterface
     */
    private $service;


    /**
     * UserController constructor.
     * @param EmployeeServiceInterface $service
     */
    public function __construct(EmployeeServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function store()
    {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function update(int $id)
    {
        try {
            return $this->service->update($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return string
     */
    public function updateStatus(int $id)
    {
        try {
            return $this->service->updateStatus($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function details($id)
    {
        try {
            return $this->service->details($id);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function active()
    {
        try {
            return $this->service->findActive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function inactive()
    {
        try {
            return $this->service->findInactive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }

    /**
     * @return string
     */
    public function allWaiterActive()
    {
        try {
            return $this->service->allWaiterActive();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }
}