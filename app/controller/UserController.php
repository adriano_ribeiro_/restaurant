<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 09:20
 */

namespace app\controller;



use app\exception\ValidationException;
use app\service\user\UserServiceInterface;
use app\util\Json;
use app\util\Request;
use Throwable;

/**
 * Class UserController
 * @package app\controller
 */
class UserController extends Json implements ControllerInterface
{

    /**
     * @var UserServiceInterface
     */
    private $service;


    /**
     * UserController constructor.
     * @param UserServiceInterface $service
     */
    public function __construct(UserServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @return string
     */
    public function index()
    {
        try {
            return $this->service->home();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function auth() {
        try {
            return $this->service->auth();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function logout()
    {
        try {
            return $this->service->logout();
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @return string
     */
    public function store() {
        try {
            return $this->service->save($_POST);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function update(int $id)
    {
        try {
            if (Request::request('post')) {
                return $this->service->update($id);
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }


    /**
     * @param int $id
     */
    public function updateStatus(int $id)
    {
        // TODO: Implement updateStatus() method.
    }

    /**
     *
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     *
     */
    public function find()
    {
        // TODO: Implement find() method.
    }

    /**
     * @return array
     */
    public function active()
    {
        try {
            if (Request::request('get')) {
                return $this->service->active();
            }
        } catch (ValidationException $e) {
            return $this->warning($e->getCode(), $e->getMessage(), $e->getMessage());
        } catch (Throwable $e) {
            return $this->error($e->getCode(), $e->getMessage(), 'Erro no sevidor');
        }
    }

    /**
     *
     */
    public function inactive()
    {
        // TODO: Implement inactive() method.
    }
}