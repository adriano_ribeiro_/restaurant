<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 25/01/2019
 * Time: 09:20
 */

namespace app\view;


use Aura\Session\Session;


/**
 * Class UserView
 * @package app\view
 */
class UserView extends ContainerView
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * UserView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function index()
    {
        echo $this->render('home.html.twig');
    }

    public function login()
    {
        echo $this->render('user/login.html.twig');
    }

    public function register()
    {
        echo $this->render('user/register.html.twig');
    }

    public function client()
    {
        //echo $this->render('user/client.html.twig');
        echo $this->render('app/advanced-components.php');
    }

    public function users()
    {
        echo $this->render('user/users.html.twig');
    }
}