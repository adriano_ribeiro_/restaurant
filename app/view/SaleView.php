<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/04/2019
 * Time: 17:07
 */

namespace app\view;


use app\controller\SaleControllerInterface;
use app\model\Sale;
use Aura\Session\Session;


/**
 * Class SaleView
 * @package app\view
 */
class SaleView extends ContainerView
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var SaleControllerInterface
     */
    protected $controller;

    /**
     * UserView constructor.
     * @param Session $session
     * @param SaleControllerInterface $controller
     */
    public function __construct(Session $session, SaleControllerInterface $controller)
    {
        parent::__construct();
        $this->session = $session;
        $this->controller = $controller;
    }

    public function sale()
    {
        echo $this->render('sale/sale.html.twig'
//            ,
//            [
//                'sale', null,
//                'value_all', null
//            ]
        );
    }

    public function openSale()
    {
        echo $this->render('sale/open_sale.html.twig');
    }

    public function closedSale()
    {
        echo $this->render('sale/closed_sale.html.twig');
    }

//    public function getValueBySale($id)
//    {
//        /** @var Sale $sale */
//        $sale = $this->controller->getValueBySale($id);
//        $valueAll = 'R$ ' . number_format($sale->getValueAll(), 2, ',', '.');
//
//        echo $this->render('sale/sale.html.twig');
//    }

    public function detailsSaleActive()
    {
//        /** @var Sale $sale */
//        $sale = $this->controller->findById($_REQUEST['sale']);
//        echo $this->render('sale/sale.html.twig',
//            [
//                'sale' => $sale
//            ]
//        );
    }

}