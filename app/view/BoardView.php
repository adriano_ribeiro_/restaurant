<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 31/01/2019
 * Time: 14:16
 */

namespace app\view;


use Aura\Session\Session;

/**
 * Class BoardView
 * @package app\view
 */
class BoardView extends ContainerView
{

    /**
     * @var Session
     */
    protected $session;

    /**
     * UserView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function create()
    {
        echo $this->render('board/create.html.twig');
    }

    public function active()
    {
        echo $this->render('board/boards.html.twig');
    }
}