<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 06/02/2019
 * Time: 19:17
 */

namespace app\view;


use Aura\Session\Session;


/**
 * Class BoxView
 * @package app\view
 */
class BoxView extends ContainerView
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * UserView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function box()
    {
        echo $this->render('box/box.html.twig');
    }
}