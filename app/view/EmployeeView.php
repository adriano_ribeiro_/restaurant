<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 02/03/2019
 * Time: 13:50
 */

namespace app\view;


use Aura\Session\Session;


/**
 * Class EmployeeView
 * @package app\view
 */
class EmployeeView extends ContainerView
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * EmployeeView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function create()
    {
        echo $this->render('employee/create.html.twig');
    }

    public function active()
    {
        echo $this->render('employee/employees.html.twig');
    }
}