<?php
/**
 * Created by PhpStorm.
 * User: adriano
 * Date: 31/01/2019
 * Time: 14:26
 */

namespace app\view;


use Aura\Session\Session;


/**
 * Class ProductView
 * @package app\view
 */
class ProductView extends ContainerView
{

    /**
     * @var Session
     */
    protected $session;

    /**
     * UserView constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        parent::__construct();
        $this->session = $session;
    }

    public function create()
    {
        echo $this->render('product/create.html.twig');
    }

    public function products()
    {
        echo $this->render('product/products.html.twig');
    }
}