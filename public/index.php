<?php
/**
 * Date: 22/01/2019
 */

if (session_id() == '' || !isset($_SESSION)) {
    session_start();
}

date_default_timezone_set('America/Fortaleza');

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


$container = require __DIR__ . '/../bootstrap/container.php';

$container->get('SharedContainerTwig');

$dispatcher = require base_path('routes/web.php');

$httpMethod = $_SERVER['REQUEST_METHOD'];
$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$route = $dispatcher->dispatch($httpMethod, $uri);
$session = $container->get(\Aura\Session\Session::class);

switch ($route[0]) {
    case \FastRoute\Dispatcher::NOT_FOUND:
        {
            echo "Rota não encontrada";
            $view = new \app\view\UserView($session);
            return $view->login();
            break;
        }
    case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
        {
            echo "Método HTTP não permitido";
            break;
        }
    case \FastRoute\Dispatcher::FOUND:
        {
            $controller = $route[1];
            $params = $route[2];

            if (isset($route[1][2]) && $route[1][2] === 'auth') {
                $segment = $session->getSegment('Logged');
                if (!$segment->get('user')) {
                    $view = new \app\view\UserView($session);
                    return $view->login();
                }else {
                    $controller = [$route[1][0], $route[1][1]];
                    $parameters = $route[2];
                }
            }
            $container->call($controller, $params);
            break;
        }

}