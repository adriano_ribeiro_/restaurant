/**
 * 'FUNCIONÁRIOS'
 * */
var employee = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    select_functions: '#function_id',
    updateBtn: '#btn-update',

    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        name: null,
        function_id: null,
        status: null
    },

    index: function () {
        employee.loadFunctions();
        employee.validador();
        employee.tableActive();
        employee.tableInactive();
        employee.btnUpdate();
    },

    // Carrega todos os funcionários
    loadFunctions: function () {
        $.get(uri.functions.active, function (response) {
            $(employee.select_functions).html();
            let cmb = "<option value='' selected>SELECIONE A FUNÇÃO</option>";
            $.each(response.data, function (i, value) {
                cmb = cmb + '<option value="' + value.id + '">'
                    + value.name + '</option>';
            });
            $(employee.select_functions).append(cmb);
        });
    },

    // Salva um novo funcionário
    save: function () {
        this.data = $(employee.formCreate).serialize();
        $.post(uri.employee.create, this.data, function (response) {
            tool.response(response, 'Funcionário', function () {
                toastr.success('Funcionário ' + response.data + ' criado com sucesso!', 'Cadastrando...');
                $(employee.formCreate).trigger("reset");
            });
        });
    },

    // valida os inputs do formulário.
    validador: function () {
        $(employee.formCreate).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                function_id: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'Campo Nome Obrigatório',
                    minlength: 'Digite no mínimo 3 Caracteres'
                },
                function_id: {
                    required: 'Campo Função Obrigatório'
                }
            },
            submitHandler: function () {
                employee.save();
                return false;
            }
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, name, function_id) {
        $('#update-id').val(id);
        $('#update-name').val(name);
        tool.setValueOptionSelect2ByText(function_id, 'function_id');
    },

    // Preencher modal para edição do funcionário pelo o id
    modalUpdate: function (id) {
        $.get(uri.employee.details + id, null, function (response) {
            tool.response(response, 'Funcionário', function () {
                $(employee.updateModal).modal('show');
                employee.id = response.data.id;
                employee.name = response.data.name;
                employee.function_id = response.data.function_id;
                console.log(response.data.function_id);
                employee.detailsUpdate(employee.id, employee.name, employee.function_id);
            });
        });
    },

    // aqui vc atualiza os dados do funcionário
    update: function (name, function_id) {
        this.data = {
            'name': name,
            'function_id': function_id
        };

        $.post(uri.employee.update + employee.id, this.data, function (response) {
            tool.response(response, 'Funcionário', function () {
                toastr.success('Funcionário ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-name').val('');
                $('#function_id').val('');
                $(employee.updateModal).modal('hide');
                employee.reloadTable();
            });
        });
    },

    btnUpdate: function () {
        $(employee.updateBtn).click(function () {
            employee.name = $('#update-name').val();
            employee.function_id = $('#function_id').val();
            if (employee.name !== '', employee.function_id !== '') {
                return employee.update(employee.name, employee.function_id);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc tanto ativa como desativa um funcionário, a ação é feito nas tabelas
    updateStatus: function (id, status) {
        this.data = {'status': status};
        $.post(uri.employee.updateStatus + id, this.data, function (response) {
            tool.response(response, 'Funcionário', function () {
                toastr.success('Status do Funcionário ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                employee.reloadTable();
            });
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#active').DataTable().ajax.reload();
    reloadTable: function () {
        employee.active.ajax.reload();
        employee.inactive.ajax.reload();
    },

    tableActive: function () {
        employee.active = $('#active').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.employee.active,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "function_id"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-warning fas fa-edit color-black btn-sm'> Editar</button>";
                    },
                    targets: 2,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='disable btn-danger fas fa-lock color-black btn-sm'> Desativar</button>";
                    },
                    targets: 3,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {orderable: false, targets: [0]}
            ]
        });

        employee.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            employee.modalUpdate(id);
        });

        employee.active.on('click', '.disable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            employee.updateStatus(id, 0);
        });
    },

    tableInactive: function () {
        employee.inactive = $('#inactive').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.employee.inactive,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "function_id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='enable btn-info fas fa-unlock color-black btn-sm'> Ativar</button>";
                    },
                    targets: 2,
                    "searchable": false,
                    className: "dt-body-right"
                },
                {orderable: false, targets: [0]}
            ]
        });

        employee.inactive.on('click', '.enable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            employee.updateStatus(id, 1);
        });
    },
}