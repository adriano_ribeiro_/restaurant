var board = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadBoardsModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        name: null,
        status: null
    },

    index: function () {
        board.validador();
        board.tableActive();
        board.tableInactive();
        board.btnUpdate();
        $('#name').focus();
    },

    // Aqui vc salva uma mesa
    save: function () {
        this.data = $(board.formCreate).serialize();
        $.post(uri.board.create, this.data, function (response) {
            tool.response(response, 'Mesa ', function () {
                toastr.success('Mesa ' + response.data + ' criado com sucesso!', 'Cadastrando . . .');
                $(board.formCreate).trigger("reset");
            });
        });
    },

    // Aqui vc valida os dados do formulário antes de salvar uma mesa
    validador: function () {
        $(board.formCreate).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 2
                },
            },
            messages: {
                name: {
                    required: 'Campo N° Da Mesa Obrigatório',
                    minlength: 'Digite no mínimo 2 Caracteres ex: 01'
                }
            },
            submitHandler: function () {
                board.save();
                return false;
            }
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, name) {
        $('#update-id').val(id);
        $('#update-name').val(name);
    },

    // Preencher modal para edição da mesa pelo o id
    modalUpdate: function (id) {
        $.get(uri.board.details + id, null, function (response) {
            tool.response(response, 'Mesa ', function () {
                $(board.updateModal).modal('show');
                board.id = response.data.id;
                board.name = response.data.name;
                board.detailsUpdate(board.id, board.name);
            });
        });
    },


    // Aqui vc edita uma mesa
    update: function (name) {
        this.data = {
            'name': name
        };

        $.post(uri.board.update + board.id, this.data, function (response) {
            tool.response(response, 'Mesa ', function () {
                toastr.success('Mesa ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-name').val('');
                $(board.updateModal).modal('hide');
                board.reloadTable();
            });
        });
    },

    // botão que faz ação para editar uma mesa.
    btnUpdate: function () {
        $(board.updateBtn).click(function () {
            board.name = $('#update-name').val();
            if (board.name !== '') {
                return board.update(board.name);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc tanto ativa como desativa uma mesa pq a ação é feito nas tabelas
    updateStatus: function (id, status) {
        this.data = {'status': status};
        $.post(uri.board.updateStatus + id, this.data, function (response) {
            tool.response(response, 'Mesa ', function () {
                toastr.success('Status da Mesa ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                board.reloadTable();
            });
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#boardActive').DataTable().ajax.reload();
    reloadTable: function () {
        board.active.ajax.reload();
        board.inactive.ajax.reload();
    },

    tableActive: function () {
        board.active = $('#active').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.board.active,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-warning fas fa-edit color-black btn-sm'> Editar</button>";
                    },
                    targets: 1,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='disable btn-danger fas fa-lock color-black btn-sm'> Desativar</button>";
                    },
                    targets: 2,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {orderable: false, targets: [1]}
            ]
        });

        board.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            board.modalUpdate(id);
        });

        board.active.on('click', '.disable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            board.updateStatus(id, 0);
        });
    },

    tableInactive: function () {
        board.inactive = $('#inactive').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.board.inactive,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='enable btn-info fas fa-unlock color-black btn-sm'> Ativar</button>";
                    },
                    targets: 1,
                    "searchable": false,
                    className: "dt-body-right"
                },
                {orderable: false, targets: [1]}
            ]
        });

        board.inactive.on('click', '.enable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            board.updateStatus(id, 1);
        });
    },

}

