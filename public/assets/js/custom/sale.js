/**
 * Created by adriano on 16/03/2019.
 */
var sale = {

    selectBoards: '#boards',
    selectProducts: '#products',
    selectWaiters: '#waiters',
    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    createModal: '#createModal',
    updateModal: '#updateModal',
    createBtn: '#btn-create',
    updateBtn: '#btn-update',
    saleId: '#sale_id',
    groupId: '#group_id',
    valueAllId: '#value_all',
    tableActiveId: '#active',
    tableInactiveId: '#inactive',


    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        product_id: null,
        product_name: null,
        waiter_id: null,
        waiter_name: null,
        quantity: null,
        valueUnit: null,
        valueAll: null
    },

    index: function () {
        sale.loadBoardsActive();
        // sale.checkId();
        sale.loadWaiterActive();
        sale.validadorSale();
        sale.btnCreate();
        //sale.tableSale(1);
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
                t = t == undefined ? "." : t, s = n < 0 ? "-" : "",
                i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    // retorna a quantidade de mesas ativas
    loadBoardsActive: function () {
        $.get(uri.board.active, function (response) {
            $(sale.selectBoards).html();
            let cmb = "";
            $.each(response.data, function (i, parameter) {
                cmb = cmb + '<option value="' + parameter.id + '">'
                    + parameter.name + '</option>';
            });
            $(sale.selectBoards).append(cmb);
            // console.log(response);
        });
    },

    // retorna a quantidade de produtos ativos
    loadProductsActive: function () {
        $(sale.selectProducts).select2({
            language: 'pt-BR',
            allowClear: true,
            placeholder: 'INFORME O NOME DO PRODUTO',
            ajax: {
                url: uri.product.activeSale,
                dataType: 'json',
                delay: 250,
                processResults: function (response) {
                    return {
                        results: response.data
                    };
                },
                cache: true
            }
        });
    },

    // retorna a quantidade de garçons
    loadWaiterActive: function () {
        $.get(uri.employee.allWaiterActive, function (response) {
            $(sale.selectWaiters).html();
            let cmb = "<option value='' selected>SELECIONE UM GARÇOM</option>";
            $.each(response.data, function (i, parameter) {
                cmb = cmb + '<option value="' + parameter.id + '">'
                    + parameter.name + '</option>';
            });
            $(sale.selectWaiters).append(cmb);
        });
    },

    // retorna a quantidade de vendas ativas
    countSaleActive: function () {
        $.get(uri.sale.countActive, function (response) {
            tool.response(response, 'Venda ', function () {
                return $('#quantity_sale_active').text(response.data);
            });
            return toastr.error('ERRO AO TENTAR CONTAR O NÚMERO DE VENDAS EM ABERTO', 'Erro');
        });
    },


    getBoardsBySale: function () {
        $.get(uri.sale.boards + sale.params.id, function (response) {
            tool.response(response, 'Mesas', function () {
                sale.autoSelectBoards(response.data);
            });
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#active').DataTable().ajax.reload();
    reloadTable: function () {
        sale.active.ajax.reload();
        sale.inactive.ajax.reload();
    },

    tableSale: function (id) {
        //destruir tabela
        // $(sale.tableActiveId).destroy();
        id = sale.params.id;
        sale.active = $('#active').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.sale.details + id,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "product"},
                {"data": "priceUnit"},
                {"data": "quantity"},
                {"data": "subtotal"},
                {"data": "sale"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='remove btn-danger fas fa-times color-black btn-sm'> Remover Item</button>";
                    },
                    targets: 4,
                    "searchable": false,
                    className: "dt-body-right"
                },
                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: [1, 2, 3]
                },
                {orderable: false, targets: [0]}
            ]
        });

        sale.active.on('click', '.remove', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            sale.updateStatus(id, 1);
        });

        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
                t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    getValueAll: function () {
        $.get(uri.sale.valueAll + sale.params.id, function (response) {
            console.log('Response :' + response);
            console.dir(response);
            tool.response(response, 'Valor Total', function () {
                if (response) {
                    sale.params.valueAll = 'R$ ' + response.data.valueAll.formatMoney(2, ',', '.');
                    return $(sale.valueAllId).text(sale.params.valueAll);
                }
                return $(sale.valueAllId).text('R$ 0,00');
            });
        });
    },

    loadData: function () {
        $.get(uri.sale.details + sale.params.id, function (response) {
            tool.response(response, 'Pedidos ', function () {
                if (response.data.length === 0) {
                    //sale.deleteSell();
                } else {
                    sale.formatData(response.data);
                }
            });
        });
    },

    loadProductsTable: function () {

        selectOption();

        function selectOption() {

            let uri = $(location).attr('href');
            uri = uri.split('/');
            uri = uri[3];
            uri = uri.split('/');

            if (uri[0] === "/pedidos/detalhes") {
                loadOption2();
                sale.loadData();
                $('#update_board').fadeIn(500);
                $('.hide-to-modal').fadeIn(500);
            } else if (uri[0] === "/pedidos/fechado") {
                loadOption1();
                sale.loadData();
                $('#boards').prop('disabled', true);
                $('#finish').css('display', 'none');
            } else {
                // sale.getValueAll();
                let id = sale.params.id;
                id = id ? id : 0;
                console.log('id: ' + id);
                sale.tableSale(id);
                //loadOption2();
                $('#finish').attr("disabled", true);
            }
        }

        function loadOption1() {
            sale.active = $('#active').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend: 'print',
                        message: $('#sale_id').text(),
                        text: '<i class="fa fa-print" aria-hidden="true"></i> IMPRIMIR',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4]
                        },
                        customize: function (win) {
                            $(win.document.body).find('table').addClass('display').css('font-size', '15px');
                            $(win.document.body).find('tr:nth-child(odd) td').each(function (index) {
                                $(this).css('background-color', '#D0D0D0');
                            });
                            $(win.document.body).find('h1').css('text-align', 'center');
                        }
                    }
                ],
                columnDefs: [
                    {
                        "targets": [5],
                        "visible": false,
                        "searchable": false
                    }
                ],
                "language": {
                    "url": "/assets/Portuguese-Brasil.json"
                }
            });
        }

        function loadOption2() {
            sale.active = $('#active').DataTable({
                "language": {
                    "url": "/assets/Portuguese-Brasil.json"
                },
                "columnDefs": [
                    {
                        "targets": [5],
                        "visible": false,
                        "searchable": false
                    },
                ]
            });

            Number.prototype.formatMoney = function (c, d, t) {
                var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d === undefined ? "," : d,
                    t = t === undefined ? "." : t, s = n < 0 ? "-" : "",
                    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
                return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
            };
        }
    },

    checkId: function () {
        let id = $('#sale_id').val();
        sale.params.id = id ? id : 0;
        if (sale.params.id !== 0) {
            sale.loadProductsTable();
            //$('.saleId').fadeIn(500);
            sale.getBoardsBySale();
        } else {
            sale.loadProductsTable();
        }
    },

    autoSelectBoards: function (data) {
        $(sale.selectBoards).html();
        let cmb = "";
        let boards_selectd = [];
        $.each(data, function (i, params) {
            boards_selectd.push(params.id);
            cmb = cmb + '<option value="' + params.id + '" selected>'
                + params.name + '</option>';
        });
        $(sale.selectBoards).append(cmb);
        $.get(uri.board.active, function (response) {
            tool.response(response, 'Mesas', function () {
                let cmb = "";
                $.each(response.data, function (i, params) {
                    if (jQuery.inArray(params.id, boards_selectd) == -1) {
                        cmb = cmb + '<option value="' + params.id + '">'
                            + params.name + '</option>';
                    }
                });
                $(sale.selectBoards).append(cmb);
            })
        });
    },

    btnCreate: function () {
        $(sale.createBtn).click(function () {
            $(sale.formCreate).submit();
        });
    },

    // aqui eu adiciono um item na venda
    saveSale: function () {
        this.data = $(sale.formCreate).serialize();
        $.post(uri.box.create, this.data, function (response) {
            console.log('response: '+response);
            tool.response(response, 'Produto', function () {
                toastr.success('Produto adicionado com sucesso!', 'Cadastrando...');
                $(sale.groupId).val(response.data.group);
                $(sale.saleId).val(response.data.sale);
                sale.params.id = response.data;
                $(sale.selectProducts).val(null).trigger('change');
                $('#quantity').val(1);
                sale.tableSale(sale.params.id);
                sale.getValueAll();
            });
        });
    },

    validadorSale: function () {
        $(sale.formCreate).validate({
            rules: {
                boards: {
                    required: true
                },
                products: {
                    required: true
                },
                quantity: {
                    required: true
                },
                waiters: {
                    required: true
                }
            },
            submitHandler: function () {
                sale.saveSale();
                return false;
            }
        });
    },

    saveBox: function () {
        $.post(uri.sale.box, this.data, function (response) {
            tool.response(response, 'Caixa ', function () {

            });
        });
    }

};