/**
 * Created by adriano on 17/04/2019.
 */
var saleOpen = {

    tableActiveId: '#active',
    tableInactiveId: '#inactive',
    active: null,
    inactive: null,
    data: null,

    index: function () {
        saleOpen.loadData();
    },

    addRowTable: function (params) {
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
                t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
        saleOpen.active.row.add([
            params.boards,
            'R$ ' + parseFloat(params.valueAll).formatMoney(2, ',', '.'),
            "<button href='" + params.id + "' class='update btn-info fas fa-folder-plus color-black btn-sm'><strong> ADICIONAR</strong></button>",

            "<button href='" + params.id + "' class='finish btn-danger fas fa-check color-black btn-sm'><strong> FINALIZAR</strong></button>"
        ]).draw(false);
    },

    loadTable: function (response) {

        saleOpen.active = $('#active').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            initComplete: function () {

            }
        });

        $.each(response, function (k, v) {
            saleOpen.addRowTable(v[0]);
        });
    },

    loadData: function () {
        $.get(uri.sale.active, function (response) {
            tool.response(response, 'Pedidos ', function () {
                saleOpen.loadTable(saleOpen.formatData(response.data));
            });
        });
    },

    formatData: function (data) {

        var array_reduce = data.reduce(function (field, e1) {
            var matches = field.filter(function (e2) {
                return e1.id === e2.id
            });
            if (matches.length === 0) {
                field.push(e1);
            }
            return field;
        }, []);

        var array = (JSON.stringify(array_reduce));
        array = $.parseJSON(array);

        var array_sale_open_active = [];
        $.each(array, function (key, value) {
            array_sale_open_active.push(mergeData(data, value.id));
        });

        return array_sale_open_active;

        function mergeData(data, id) {

            var boards = '';
            var array_pedido = [];

            $.each(data, function (key, value) {
                if (id === value.id) {
                    boards = boards + value.boards.toString() + ', ';
                }
            });

            boards = boards.substring(0, boards.length - 2);

            $.each(data, function (key, value) {
                if (id === value.id) {
                    var sale_open = {
                        "id": value.id,
                        "valueAll": value.valueAll
                    };
                    var sale_open_board = $.extend(sale_open, {"boards": boards});
                    array_pedido.push(sale_open_board);
                    return false;
                }
            });
            return array_pedido;
        }
    },

    actionTable: function () {

        $(saleOpen.tableActiveId).on('click', '.update', function (e) {
            e.preventDefault();
            let id = $(this).attr('href');
            window.location.href = "/pedidos_detalhes?sell_id=" + id;
        });

        $(saleOpen.tableActiveId).on('click', '.finish', function (e) {
            e.preventDefault();
            let id = $(this).attr('href');
            let tr = $(this).parents('tr');
            saleOpen.confirmFinish(id, tr);
        });
    },

    confirmFinish: function (id, tr) {
        $.confirm({
            text: "REALMENTE DESEJA FINALIZAR ESSA VENDA?",
            title: "CONFIRMAÇÃO REQUERIDA",
            confirm: function () {
                saleOpen.finishSale(id, tr);
            },
            cancel: function () {
            },
            confirmButton: "SIM EU QUERO",
            cancelButton: "NÃO",
            confirmButtonClass: "btn-primary",
            cancelButtonClass: "btn-danger",
            dialogClass: "modal-dialog"
        });
    },

    finishSale: function (id, tr) {
        $.get(uri.sale.finish + id, function (response) {
            tool.response(response, 'Venda', function () {
                toastr.success('Venda Finalizada Com Sucesso.', 'Finalizando . . .');
                saleOpen.removeRowTable(tr);
                sale.countSaleActive();
                //tool.comanda(id);
                saleOpen.active.clean().draw();
                saleOpen.active.destroy();
                saleOpen.loadData();
            });
        });
    },

    removeRowTable: function (tr) {
        saleOpen.active
            .row(tr)
            .remove()
            .draw();
    },
};