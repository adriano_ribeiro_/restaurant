var user = {

    formLogin: '#formLogin',
    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#userModal',
    modalUpdate: '#modalUpdate',
    modalDisabled: '#modalDisabled',
    select_functions: '#function',

    loadModal: null,
    tableActive: null,
    tableInactive: null,
    data: null,

    id: null,
    name: null,
    email: null,
    password: null,
    function_id: null,
    status: null,

    index: function () {

    },

    // Login do site
    login: function () {
        this.data = $(user.formLogin).serialize();
        $.post(uri.user.login, this.data, function (response) {
            if (response.code === true) {
                return setTimeout('window.location.href = uri.user.read', 400);
            }
            if (response.data === false) {
                return toastr.warning('Email ou Senha inválidos', 'Advertência');
            }
            return toastr.error(response.message, 'Erro');
        });
    },
}