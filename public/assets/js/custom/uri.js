var uri = {

    functions: {
        create: '/function/store',
        active: '/function/active',
        inactive: '/function/inactive',
        edit: '/function/editar/',
        update: '/function/update/',
        delete: '/function/delete/',
        details: '/function/details/'
    },

    user: {
        login: '/user/login',
        create: '/user/store',
        read: '/',
        statusTrue: '/user/status/true',
        statusFalse: '/user/status/false',
        home: '/home',
        edit: '/usuario/editar/',
        update: '/user/update/',
        updateStatus: '/user/update/status/',
        delete: '/user/delete/',
        details: '/user/details/',
        functionsActive: '/user/function/active'
    },

    employee: {
        create: '/employee/store',
        update: '/employee/update/',
        updateStatus: '/employee/update/status/',
        active: '/employee/active',
        inactive: '/employee/inactive',
        allWaiterActive: '/employee/all/waiter/active',
        edit: '/employee/editar/',
        delete: '/employee/delete/',
        details: '/employee/details/'
    },

    board: {
        create: '/board/store',
        update: '/board/update/',
        active: '/board/active',
        inactive: '/board/inactive',
        details: '/board/details/',
        edit: '/board/editar/',
        updateStatus: '/board/update/status/',
        delete: '/board/delete/'
    },

    product: {
        create: '/product/store',
        update: '/product/update/',
        active: '/product/active',
        inactive: '/product/inactive',
        activeSale: '/product/active/sale',
        details: '/product/details/',
        updateStatus: '/product/update/status/',
        delete: '/product/delete/',
        edit: '/product/editar/'
    },

    box: {
        create: '/box/store',
    },

    sale: {
        box: '/sale/box',
        create: '/sale/store',
        active: '/sale/active',
        inactive: '/sale/inactive',
        boards: '/boards/',
        details: '/sale/details/',
        edit: '/sale/editar/',
        update: '/sale/update/',
        updateStatus: '/sale/update/status/',
        delete: '/sale/delete/',
        countActive: '/sale/count/active',
        valueAll: '/sale/value/all/',
        finish: '/sale/finish/'
    },

    group: {
        active: '/group/sale/active',
        inactive: '/group/sale/inactive',
        details: 'group/sale/details/',
    },
};