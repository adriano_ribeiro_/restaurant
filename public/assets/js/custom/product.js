/**
 * product.js 19/03/2019
 */
var product = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    detailsModal: '#detailsModal',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadProductModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        name: null,
        description: null,
        price: null,
        status: null
    },

    index: function () {
        product.validador();
        product.btnUpdate();
        product.tableActive();
        product.tableInactive();
        $('#name').focus();
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
                t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    save: function () {
        product.price = $('#price').val();
        product.price = product.price.split(' ')[1];
        this.data = {
            'name': $('#name').val(),
            'description': $('#description').val(),
            'price': tool.convertMoedaToFloat(product.price)
        };
        $.post(uri.product.create, this.data, function (response) {
            console.log(response);
            tool.response(response, 'Produto ', function () {
                toastr.success('Produto ' + response.data + ' Adicionado Com Sucesso!', 'Adicionando . . .');
                $(product.formCreate).trigger("reset");
            });
        });
    },

    validador: function () {
        $(product.formCreate).validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                description: {
                    required: true,
                    minlength: 3
                },
                price: {
                    required: true,
                    minlength: 1
                },
            },
            messages: {
                name: {
                    required: 'Campo Nome Obrigatório',
                    minlength: 'Digite no mínimo 3 Caracteres'
                },
                description: {
                    required: 'Campo Descrição Obrigatório',
                    minlength: 'Digite no mínimo 3 Caracteres'
                },
                price: {
                    required: 'Campo Preço Obrigatório'
                }
            },
            submitHandler: function () {
                product.save();
                return false;
            }
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, name, description, price) {
        $('#update-id').val(id);
        $('#update-name').val(name);
        $('#update-description').val(description);
        $('#update-price').val(price);
    },

    // Preencher modal para edição do produto pelo o id
    modalUpdate: function (id) {
        $.get(uri.product.details + id, null, function (response) {
            tool.response(response, 'Produto ', function () {
                $(product.updateModal).modal('show');
                product.id = response.data.id;
                product.name = response.data.name;
                product.description = response.data.description;
                product.price = 'R$ ' + response.data.price.formatMoney(2, ',', '.');
                console.log(product.price);
                product.detailsUpdate(product.id, product.name, product.description, product.price);
            });
        });
    },

    update: function (name, description, price) {
        price = price.split(' ')[1];
        this.data = {
            'name': name,
            'description': description,
            'price': tool.convertMoedaToFloat(price),
        };

        $.post(uri.product.update + product.id, this.data, function (response) {
            tool.response(response, 'Produto ', function () {
                toastr.success('Produto ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-name').val('');
                $('#update-description').val('');
                $('#update-price').val('');
                $(product.updateModal).modal('hide');
                product.reloadTable();
            });
        });
    },

    btnUpdate: function () {
        $(product.updateBtn).click(function () {
            product.name = $('#update-name').val();
            product.description = $('#update-description').val();
            product.price = $('#update-price').val();
            if (product.name !== '', product.description !== '', product.price !== '') {
                return product.update(product.name, product.description, product.price);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc tanto ativa como desativa uma mesa pq a ação é feito nas tabelas
    updateStatus: function (id, status) {
        this.data = {'status': status};
        $.post(uri.product.updateStatus + id, this.data, function (response) {
            tool.response(response, 'Produto ', function () {
                toastr.success('Status do Produto ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                product.reloadTable();
            });
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#active').DataTable().ajax.reload();
    reloadTable: function () {
        product.active.ajax.reload();
        product.inactive.ajax.reload();
    },

    tableActive: function () {
        product.active = $('#active').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.product.active,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "description"},
                {"data": "price"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-warning fas fa-edit color-black btn-sm'> Editar</button>";
                    },
                    targets: 3,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='disable btn-danger fas fa-lock color-black btn-sm'> Desativar</button>";
                    },
                    targets: 4,
                    "searchable": false
                    // className: "dt-body-center"
                },
                {
                    render: function (data) {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: 2
                },
                {orderable: false, targets: [0]}
            ]
        });

        product.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            product.modalUpdate(id);
        });

        product.active.on('click', '.disable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            product.updateStatus(id, 0);
        });
    },

    tableInactive: function () {
        product.inactive = $('#inactive').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.product.inactive,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='enable btn-info fas fa-unlock color-black btn-sm'> Ativar</button>";
                    },
                    targets: 1,
                    "searchable": false,
                    className: "dt-body-right"
                },
                {orderable: false, targets: [0]}
            ]
        });

        product.inactive.on('click', '.enable', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            product.updateStatus(id, 1);
        });
    },
}