<?php

use app\factory\ProductFactory;
use app\model\Product;
use app\provider\Connection;
use app\repository\product\ProductRepository;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;


class ProductTest extends TestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /** @var Product */
    private $product;

    /**
     * @var ProductRepository
     */
    private $repository;

    public function conn()
    {
        return Connection::conn();
    }

    /**
     * @dataProvider collectionsData
     * @param $param
     * @param $setValue
     * @param $valueActual
     */
    public function testEncapsulate($param, $setValue, $valueActual)
    {
        $this->product = new Product();
        $result = $this->product->{'set' . ucfirst($param)}($valueActual);
        $this->assertInstanceOf(Product::class, $result);
        $valueActual = $this->product->{'get' . ucfirst($param)}();
        $this->assertEquals($setValue, $valueActual);
    }

    public function collectionsData()
    {
        return [
            ['name', 'skol 300 ml', 'skol 300 ml'],
            ['description', 'refrigerante', 'refrigerante'],
            ['price', 8.99, 8.99]
        ];
    }


    public function testIsArrayObject()
    {
        $this->em = $this->conn();
        $this->repository = $this->em->getRepository(Product::class);
        $this->assertIsArray($this->repository->findActive());
    }

    public function testIsObject()
    {
        $this->em = $this->conn();
        $this->repository = $this->em->getRepository(Product::class);
        $this->assertNull($this->repository->getReference(10));
        $this->assertIsObject($this->repository->getReference(1));
    }

    public function testIsSave()
    {
        $this->em = $this->conn();
        $this->repository = $this->em->getRepository(Product::class);
        $this->product = ProductFactory::make([
            'name' => 'batata',
            'description' => 'batatinha',
            'price' => 10.99
        ]);

        $this->repository->save($this->product);

        $this->assertEquals(11, $this->product->getId());
        $this->assertEquals('batata', $this->product->getName());
        $this->assertEquals('batatinha', $this->product->getDescription());
        $this->assertEquals(10.99, $this->product->getPrice());
    }

    public function testIsUpdate()
    {
        $this->product = new Product();
        $this->em = $this->conn();
        $this->repository = $this->em->getRepository(Product::class);
        $this->product = $this->repository->getReference(11);

        $this->product
            ->setName('batatinha')
            ->setDescription('batatinha frita')
            ->setPrice(11.99)
            ;

        $this->repository->save($this->product);

        $this->assertEquals(11, $this->product->getId());
        $this->assertEquals('batatinha', $this->product->getName());
        $this->assertEquals('batatinha frita', $this->product->getDescription());
        $this->assertEquals(11.99, $this->product->getPrice());

    }

    public function testIsDeleted()
    {
        $this->product = new Product();
        $this->em = $this->conn();
        $this->repository = $this->em->getRepository(Product::class);
        $this->product = $this->repository->getReference(11);

        $this->assertEquals(11, $this->product->getId());
        $this->assertEquals('batatinha', $this->product->getName());
        $this->assertEquals('batatinha frita', $this->product->getDescription());
        $this->assertEquals(11.99, $this->product->getPrice());

        $this->repository->delete($this->product->getId());
    }
}