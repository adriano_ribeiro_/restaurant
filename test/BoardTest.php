<?php

declare(strict_types=1);

use app\model\Board;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    public function testSetAndGetName()
    {
        $board = new Board();
        $this->assertInstanceOf(Board::class, $board->setName('06'));
        $this->assertEquals('06', $board->getName());
        $this->assertIsString($board->getName());
    }

    public function testSetAndGetStatus()
    {
        $board = new Board();
        $this->assertInstanceOf(Board::class, $board->setStatus(true));
        $this->assertEquals(true, $board->isStatus());
    }

    public function testSetAndGetCreatedAt()
    {
        $board = new Board();
        $board->setCreatedAt();
        $this->assertInstanceOf(Board::class, $board);
        $this->assertEquals($board->getCreatedAt(), $board->getCreatedAt());
    }

    public function testSetAndGetUpdatedAt()
    {
        $board = new Board();
        $board->setUpdatedAt();
        $this->assertInstanceOf(Board::class, $board);
        $this->assertEquals($board->getUpdatedAt(), $board->getUpdatedAt());
    }

    public function testSetAndGetGroup()
    {
        $board = new Board();
        $g = new \app\model\Group();
        $this->assertInstanceOf(Board::class, $board->setGroup($g));
        $this->assertEquals($board->getCreatedAt(), $board->getCreatedAt());
    }

    public function testString()
    {
        $board = new Board();
        $board->setName('21');
        $this->assertIsString($board->getName());
    }

    public function testBoolean()
    {
        $board = new Board();
        $board->setStatus(true);
        $this->assertIsBool($board->isStatus());
    }

    /**
     * @dataProvider collectionsNames
     * @param $name
     */
    public function testName($name)
    {
        echo $name;
    }

    public function collectionsNames()
    {
        $board = new Board();
        $board->setName('01');
        $board->setStatus(true);
        return [
            ['name' => $board->getName()]
        ];
    }

    /**
     * @dataProvider collectionsData
     * @param $param
     * @param $setValue
     * @param $valueActual
     */
    public function testEncapsulate($param, $setValue, $valueActual)
    {
        $board = new Board();
        $result = $board->{'set'.ucfirst($param)}($valueActual);
        $this->assertInstanceOf(Board::class, $result);
        $valueActual = $board->{'get'.ucfirst($param)}();
        $this->assertEquals($setValue, $valueActual);
    }

    public function collectionsData()
    {
        return [
            ['name', '01', '01']
        ];
    }
}