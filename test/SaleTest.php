<?php

declare(strict_types=1);


use app\model\Sale;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class SaleTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var Sale */
    private $sale;

    public function testFindById()
    {
        $id = 1;
        $this->sale = $this->em->getRepository(Sale::class)->findById($id);

        $this->assertIsObject($this->sale);
    }
}