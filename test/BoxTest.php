<?php

declare(strict_types=1);


use app\model\Sale;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class BoxTest extends TestCase
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /** @var Sale */
    private $sale;

    public function testSaveBox()
    {

        $this->sale = $this->em->getRepository(Sale::class);

        $this->assertIsObject($this->sale);
    }
}