<?php


use app\model\Employee;
use app\model\FunctionPerson;
use app\model\Person;
use app\provider\Connection;
use app\repository\employee\EmployeeRepository;
use app\repository\functionPerson\FunctionPersonRepository;
use app\repository\person\PersonRepository;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class EmployeeTest extends TestCase
{
    /**
     * @var EntityManager
     */
    private $em;

    /** @var Person */
    private $person;

    /** @var Employee */
    private $employee;

    /**
     * @var PersonRepository
     */
    private $repoPerson;

    /**
     * @var EmployeeRepository
     */
    private $repoEmployee;

    /**
     * @var FunctionPersonRepository
     */
    private $repoFunction;

    public function conn()
    {
        return Connection::conn();
    }

//    /**
//     * @dataProvider collectionsData
//     * @param $param
//     * @param $setValue
//     * @param $valueActual
//     */
//    public function testEncapsulate($param, $setValue, $valueActual)
//    {
//        $employee = new Employee();
//        $result = $employee->getPerson()->{'set'.ucfirst($param)}($valueActual);
//        $this->assertInstanceOf(Employee::class, $result);
//        $valueActual = $employee->getPerson()->{'get'.ucfirst($param)}();
//        $this->assertEquals($setValue, $valueActual);
//    }
//
//    public function collectionsData()
//    {
//        return [
//            ['name', '01', '01']
//        ];
//    }

    public function testSave()
    {
        $this->em = $this->conn();
        $this->repoFunction = $this->em->getRepository(FunctionPerson::class);
        $this->repoPerson = $this->em->getRepository(Person::class);
        $this->repoEmployee = $this->em->getRepository(Employee::class);
        /** @var FunctionPerson $function */
        $function = $this->repoFunction->getReference(3);
        $this->person = new Person();
        $this->employee = new Employee();

        $this->person
            ->setName('emanoel sales')
            ->setStatus(true)
            ->setFunction($function)
        ;
        $this->assertInstanceOf(Person::class, $this->person);

        $this->repoPerson->save($this->person);

        /** @var Person $person */
        $person = $this->repoPerson->getReference($this->person->getId());

        $this->employee
            ->setPerson($person);

        $this->assertInstanceOf(Employee::class, $this->employee);

        $this->repoEmployee->save($this->employee);

        $this->assertEquals(11, $this->person->getId());
        $this->assertEquals('emanoel sales', $this->employee->getPerson()->getName());

    }
}