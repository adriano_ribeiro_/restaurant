<?php


use app\controller\BoardController;
use app\controller\BoxController;
use app\controller\EmployeeController;
use app\controller\FunctionPersonController;
use app\controller\GroupController;
use app\controller\ProductController;
use app\controller\SaleController;
use app\controller\SaleControllerInterface;
use app\controller\UserController;
use app\provider\Connection;
use app\provider\View;
use app\service\board\BoardService;
use app\service\board\BoardServiceInterface;
use app\service\box\BoxService;
use app\service\box\BoxServiceInterface;
use app\service\employee\EmployeeService;
use app\service\employee\EmployeeServiceInterface;
use app\service\functionPerson\FunctionPersonService;
use app\service\functionPerson\FunctionPersonServiceInterface;
use app\service\group\GroupService;
use app\service\group\GroupServiceInterface;
use app\service\person\PersonService;
use app\service\person\PersonServiceInterface;
use app\service\product\ProductService;
use app\service\product\ProductServiceInterface;
use app\service\sale\SaleService;
use app\service\sale\SaleServiceInterface;
use app\service\user\UserService;
use app\service\user\UserServiceInterface;
use app\util\Auth;
use app\util\TwigFunctions;
use app\view\BoardView;
use app\view\BoxView;
use app\view\EmployeeView;
use app\view\ProductView;
use app\view\SaleView;
use app\view\UserView;
use Aura\Session\Session;
use function DI\get;
use Doctrine\ORM\EntityManagerInterface;


return [

    EntityManagerInterface::class => function () {
        return Connection::conn();
    },

    View::class => \Di\autowire(View::class),
    'SharedContainerTwig' => function (\Psr\Container\ContainerInterface $container) {
        TwigFunctions::setContainer($container);
    },

    // configuração das views
    UserView::class => \Di\autowire(UserView::class)->constructor(get(Session::class)),
    EmployeeView::class => \Di\autowire(EmployeeView::class)->constructor(get(Session::class)),
    BoardView::class => \Di\autowire(BoardView::class)->constructor(get(Session::class)),
    ProductView::class => \Di\autowire(ProductView::class)->constructor(get(Session::class)),
    BoxView::class => \Di\autowire(BoxView::class)->constructor(get(Session::class)),
    SaleView::class => \Di\autowire(SaleView::class)->constructor(get(Session::class), get(SaleControllerInterface::class)),

    ############################################################################################################


    // configuração dos service e controllers

    PersonServiceInterface::class => \Di\autowire(PersonService::class)->constructor(get(EntityManagerInterface::class)),

    UserServiceInterface::class => \Di\autowire(UserService::class)->constructor(
        get(EntityManagerInterface::class), get(Session::class), get(PersonServiceInterface::class)),
    UserController::class => \Di\autowire(UserController::class)->constructor(get(UserServiceInterface::class)),

    EmployeeServiceInterface::class => \Di\autowire(EmployeeService::class)->constructor(
        get(EntityManagerInterface::class), get(PersonServiceInterface::class)),
    EmployeeController::class => \Di\autowire(EmployeeController::class)->constructor(get(EmployeeServiceInterface::class)),


    FunctionPersonServiceInterface::class => \Di\autowire(FunctionPersonService::class)->constructor(get(EntityManagerInterface::class)),
    FunctionPersonController::class => \DI\autowire(FunctionPersonController::class)->constructor(get(FunctionPersonServiceInterface::class)),

    BoardServiceInterface::class => \Di\autowire(BoardService::class)->constructor(get(EntityManagerInterface::class)),
    BoardController::class => \Di\autowire(BoardController::class)->constructor(get(BoardServiceInterface::class)),

    ProductServiceInterface::class => \Di\autowire(ProductService::class)->constructor(get(EntityManagerInterface::class)),
    ProductController::class => \Di\autowire(ProductController::class)->constructor(get(ProductServiceInterface::class)),

    SaleServiceInterface::class => \Di\autowire(SaleService::class)->constructor(get(EntityManagerInterface::class), get(BoardServiceInterface::class)),
    SaleController::class => \Di\autowire(SaleController::class)->constructor(get(SaleServiceInterface::class)),
    SaleControllerInterface::class => \Di\autowire(SaleController::class)->constructor(get(SaleServiceInterface::class)),

    GroupServiceInterface::class => \Di\autowire(GroupService::class)->constructor(get(EntityManagerInterface::class), get(BoardServiceInterface::class)),
    GroupController::class => \Di\autowire(GroupController::class)->constructor(get(GroupServiceInterface::class)),

    BoxServiceInterface::class => \Di\autowire(BoxService::class)
        ->constructor(
            get(EntityManagerInterface::class),
            get(BoardServiceInterface::class),
            get(SaleServiceInterface::class),
            get(GroupServiceInterface::class)
        ),
    BoxController::class => \Di\autowire(BoxController::class)->constructor(get(BoxServiceInterface::class)),

    Session::class => function (): Session {
        return (new \Aura\Session\SessionFactory())->newInstance($_COOKIE);
    },

    Auth::class => \Di\autowire(Auth::class)->constructor(get(Session::class)),

];