<?php
require_once __DIR__ . '/vendor/autoload.php';

use app\service\product\ProductService;

$conn = \app\provider\Connection::conn();

/** @var \Doctrine\ORM\EntityManager $em */
$em = $conn;

$product = new ProductService($em);

dump($product->findActive());
